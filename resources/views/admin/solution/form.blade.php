@extends('layouts.admin')

@section('content')
    <div class="container-fluid-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $solution ? 'Изменить решение № ' . $solution->id : 'Создать Задачу' }}</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ $solution ? action('Backend\SolutionController@update', $solution->id) : action('Backend\SolutionController@create') }}">
                    {{ csrf_field() }}


                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label for="message" class="col-md-4 control-label">message</label>

                        <div class="col-md-6">
                            <input id="message" type="text" class="form-control" name="message" value="{{ old('message') ?: ($solution ? $solution->message :'') }}"  autofocus>

                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                   

                   

                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $solution ? 'Изменить решение' : 'Создать решение' }}
                            </button>
                        </div>


                        <div class="col-sm-4 col-sm-offset-4 ">
                            <a href="{{ action('Backend\SolutionController@index') }}"  class="btn btn-primary pull-right">
                                {{ 'Назад' }}
                            </a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
