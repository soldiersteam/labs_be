@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Решения</div>
                   
                      <div class="col-sm-10">
                          
                              
                            <form method="get" action="{{ action('Backend\SolutionController@index') }}"">
                                {{ csrf_field() }}
                               
                                <div class="col-sm-3">
                                    <select multiple name="courses[]" class="form-control tags_select2">
                                    @if(!$courses_list->isEmpty())
                                        @foreach($courses_list as $course)
                                            <option  value="{{ $course->id }}">{{$course->name}}</option>
                                        @endforeach
                                    @endif
                                    @if($courses_selected)
                                        @foreach($courses_selected as $course)
                                            <option  selected value="{{ $course->id }}">{{$course->name}}</option>
                                        @endforeach
                                    @endif
                                        
                                    </select>
                                   
                                </div>
                                <div class="col-sm-3">
                                    <select multiple name="modules[]" class="form-control tags_select2">
                                    @if(!$modules_list->isEmpty())
                                        @foreach($modules_list as $module)
                                            <option  value="{{ $module->id }}">{{$module->name}}</option>
                                        @endforeach
                                    @endif
                                      @if($modules_selected)
                                        @foreach($modules_selected as $module)
                                            <option selected value="{{ $module->id }}">{{$module->name}}</option>
                                        @endforeach
                                    @endif
                                 
                                       
                                  
                                    </select>
                                </div>

                                
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary">Добавить фильтр</button>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                                        
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Найти</button>
                                            @if(Request::get('search'))
                                                <a class="btn btn-danger" href="{{ action('Backend\SolutionController@index') }}">Отменить</a>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </form>
                   

                    </div>

                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>state</th>
                        <th>status</th>
                        <th>testing_mode</th>
                        <th>actions</th>
                        <th>message</th>
                        <th>user_id</th>
                        <th>course_id</th>
                        <th>module_id</th>
                        <th>success_percentage</th>
                        <th>reviewed</th>
                        <th>problem_id</th>
                        <th>programming_language_id</th>
                        <th>testing_server_id</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($solutions as $solution)

                        <tr>
                            <td> {{$solution -> id }}</td>
                            <td> {{$solution -> state }}</td>
                            <td> {{$solution -> status }}</td>
                            <td> {{$solution -> testing_mode }}</td>
                            <td> {{$solution -> actions }}</td>
                            <td> {{$solution -> message }}</td>
                            <td> {{$solution -> user_id }}</td>
                            <td> {{$solution -> course_id }}</td>
                            <td> {{$solution -> module_id }}</td>
                            <td> {{$solution -> success_percentage }}</td>
                            <td> {{$solution -> reviewed }}</td>
                            <td> {{$solution -> problem_id }}</td>
                            <td> {{$solution -> programming_language_id }}</td>
                            <td> {{$solution -> testing_server_id }}</td>
                            <td> {{$solution -> created_at }}</td>
                            <td> {{$solution -> updated_at }}</td>
                            <td style="text-align: right; width: 187px">
                                 <a class="btn btn-primary" href="{{ action('Backend\SolutionController@showForm', $solution->id) }}">Изменить</a>
                                <form method="post" style="display: inline" action="{{ action('Backend\SolutionController@delete', $solution->id) }}" class="delete_form_{{ $solution->id }}">
                                    {{ csrf_field() }}
                                    <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $solution->id }}').submit();" >Удалить</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
               {{ $solutions->links() }}
            </div>
        </div>
    </div>
@endsection
