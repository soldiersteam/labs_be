@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Список серверов</div>
                    <div class="col-sm-3"><a class="btn btn-primary" href="{{ action('Backend\ServerController@showForm') }}">Добавить сервер</a></div>
                    <form method="get">
                        <div class="input-group col-sm-6">
                            <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Найти</button>
                                @if(Request::get('search'))
                                    <a class="btn btn-danger" href="{{ action('Backend\ServerController@index') }}">Отменить</a>
                                @endif
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>login</th>
                        <th>deleted_at</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($servers as $server)
                        <tr>
                            <td>{{ $server->id }}</td>
                            <td>{{ $server->name }}</td>
                            <td>{{ $server->login }}</td>
                            <td>{{ $server->deleted_at }}</td>
                            <td>{{ $server->created_at }}</td>
                            <td>{{ $server->updated_at }}</td>
                            <td style="text-align: right; width: 187px">
                                <a class="btn btn-primary" href="{{ action('Backend\ServerController@showForm', $server->id) }}">Изменить</a>
                                <form method="post" style="display: inline" action="{{ action('Backend\ServerController@delete', $server->id) }}" class="delete_form_{{ $server->id }}">
                                    {{ csrf_field() }}
                                    <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $server->id }}').submit();" >Удалить</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $servers->links() }}
            </div>
        </div>
    </div>
@endsection
