@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $server ? 'Изменить сервер № ' . $server->id : 'Создать сервер' }}</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ $server ? action('Backend\ServerController@update', $server->id) : action('Backend\ServerController@create') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ?: ($server ? $server->name :'') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <label for="login" class="col-md-4 control-label">Login</label>

                        <div class="col-md-6">
                            <input id="login" type="text" class="form-control" name="login" value="{{ old('login') ?: ($server ? $server->login :'') }}" required autofocus>

                            @if ($errors->has('login'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="text" class="form-control" name="password" value="" required autofocus>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $server ? 'Изменить сервер' : 'Создать сервер' }}
                            </button>
                        </div>


                        <div class="col-sm-4 col-sm-offset-4 ">
                            <a href="{{ action('Backend\ServerController@index') }}"  class="btn btn-primary pull-right">
                                {{ 'Назад' }}
                            </a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
