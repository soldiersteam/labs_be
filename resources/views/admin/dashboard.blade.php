@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">@include('admin.charts.moneyChart')</div>
            <div class="col-sm-6">@include('admin.charts.userChart')</div>
            <div class="col-sm-6">@include('admin.charts.solutionChart')</div>
        </div>
    </div>
@endsection
