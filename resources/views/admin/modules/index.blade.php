@extends('layouts.admin')

@section('content')
    <div class="container-fluid-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Модули в курсе №{{ Request::route('id') }}</div>
                    <div class="col-sm-3"><a class="btn btn-primary" href="{{ action('Backend\ModuleController@showForm', [Request::route('id')]) }}">Добавить Модуль</a></div>

                    <form method="get">
                        <div class="input-group col-sm-6">
                            <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Найти</button>
                                @if(Request::get('search'))
                                    <a class="btn btn-danger" href="{{ action('Backend\ModuleController@index', [Request::route('id')]) }}">Отменить</a>
                                @endif
                        </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>description</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($modules as $module)
                        <tr>
                            <td>{{ $module->id }}</td>
                            <td>{{ $module->name }}</td>
                            <td>{{ $module->description }}</td>
                            <td style="text-align: right; width: 268px">
                                <a class="btn btn-default" href="{{ action('Backend\ModuleProblemController@index', [Request::route('id'), $module->id]) }}">Задачи</a>
                                <a class="btn btn-primary" href="{{ action('Backend\ModuleController@showForm', [Request::route('id'), $module->id]) }}">Изменить</a>
                                <form method="post" style="display: inline" action="{{ action('Backend\ModuleController@delete', [Request::route('id'), $module->id]) }}" class="delete_form_{{ $module->id }}">
                                    {{ csrf_field() }}
                                    <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $module->id }}').submit();" >Удалить</a>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $modules->links() }}
            </div>
        </div>
    </div>
@endsection