@extends('layouts.admin')

@section('content')
    <div class="container-fluid-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $module->exists ? 'Изменить ' . $module->name : 'Создать Модуль' }}</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST"
                      action="{{ $module->exists ? action('Backend\ModuleController@update',[ Request::route('id'), $module->id]) : action('Backend\ModuleController@create', [Request::route('id')]) }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        @include('admin.translation-tabs', ['languages' => $languages, 'model' => $module])
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $module->exists ? 'Изменить Модуль' : 'Создать Модуль' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
