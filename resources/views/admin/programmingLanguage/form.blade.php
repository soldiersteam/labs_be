@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $language ? 'Изменить язык № ' . $language->id : 'Создать язык' }}</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ $language ? action('Backend\ProgrammingLanguageController@update', $language->id) : action('Backend\ProgrammingLanguageController@create') }}">
                    {{ csrf_field() }}


                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') ?: ($language ? $language->title :'') }}" required autofocus>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('ace_mode') ? ' has-error' : '' }}">
                        <label for="ace_mode" class="col-md-4 control-label">Ace mode</label>

                        <div class="col-md-6">
                            <input id="ace_mode" type="text" class="form-control" name="ace_mode" value="{{ old('ace_mode') ?: ($language ? $language->ace_mode :'') }}" required autofocus>

                            @if ($errors->has('ace_mode'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('ace_mode') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('compiler_docker_image_name') ? ' has-error' : '' }}">
                        <label for="compiler" class="col-md-4 control-label">Compiler</label>

                        <div class="col-md-6">
                            <input id="compiler" type="text" class="form-control" name="compiler_docker_image_name" value="{{ old('compiler_docker_image_name') ?: ($language ? $language->compiler_docker_image_name :'') }}" required autofocus>

                            @if ($errors->has('compiler_docker_image_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('compiler_docker_image_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('executor_docker_image_name') ? ' has-error' : '' }}">
                        <label for="executor" class="col-md-4 control-label">Executor</label>

                        <div class="col-md-6">
                            <input id="executor" type="text" class="form-control" name="executor_docker_image_name" value="{{ old('executor_docker_image_name') ?: ($language ? $language->executor_docker_image_name :'') }}" required autofocus>

                            @if ($errors->has('executor_docker_image_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('executor_docker_image_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $language ? 'Изменить язык' : 'Создать язык' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
