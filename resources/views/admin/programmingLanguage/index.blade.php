@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Языки</div>
                    <div class="col-sm-3"><a class="btn btn-primary" href="{{ action('Backend\ProgrammingLanguageController@showForm') }}">Добавить язык</a></div>

                    <form method="get">
                        <div class="input-group col-sm-6">
                            <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Найти</button>
                                @if(Request::get('search'))
                                    <a class="btn btn-danger" href="{{ action('Backend\ProgrammingLanguageController@index') }}">Отменить</a>
                                @endif
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>title</th>
                        <th>ace mode</th>
                        <th>compiler</th>
                        <th>executor</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $language)
                        <tr>
                            <td>{{ $language->id }}</td>
                            <td>{{ $language->title }}</td>
                            <td>{{ $language->ace_mode }}</td>
                            <td>{{ $language->compiler_docker_image_name }}</td>
                            <td>{{ $language->executor_docker_image_name }}</td>
                            <td style="text-align: right; width: 187px">
                                <a class="btn btn-primary" href="{{ action('Backend\ProgrammingLanguageController@showForm', $language->id) }}">Изменить</a>
                                <form method="post" style="display: inline" action="{{ action('Backend\ProgrammingLanguageController@delete', $language->id) }}" class="delete_form_{{ $language->id }}">
                                    {{ csrf_field() }}
                                    <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $language->id }}').submit();" >Удалить</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $languages->links() }}
            </div>
        </div>
    </div>
@endsection
