@extends('layouts.admin')

@section('content')
    <div class="container-fluid-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $problem->exists ? 'Изменить Задачу № ' . $problem->id : 'Создать Задачу' }}</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST"
                      action="{{ $problem->exists ? action('Backend\ProblemController@update', $problem->id) : action('Backend\ProblemController@create') }}"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('archive') ? ' has-error' : '' }}">
                        <label for="archive" class="col-md-4 control-label">Archive</label>

                        <div class="col-md-6">
                            <input id="archive" type="file" class="form-control" name="archive">
                            @if ($errors->has('archive'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('archive') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('difficulty') ? ' has-error' : '' }}">
                        <label for="difficulty" class="col-md-4 control-label">Difficulty</label>

                        <div class="col-md-6">
                            <select id="difficulty" class="form-control" name="difficulty">
                                @for($i = 1; $i <= 5; ++$i)
                                    <option {{ $i === $problem->difficulty ? 'selected' : '' }}>{{ $i }}</option>
                                @endfor
                            </select>

                            @if ($errors->has('difficulty'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('difficulty') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        @include('admin.translation-tabs', ['languages' => $languages, 'model' => $problem])
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $problem->exists ? 'Изменить Задачу' : 'Создать Задачу' }}
                            </button>
                        </div>

                        <div class="col-sm-4 col-sm-offset-4 ">
                            <a href="{{ action('Backend\ProblemController@index') }}"
                               class="btn btn-primary pull-right">
                                {{ 'Назад' }}
                            </a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
