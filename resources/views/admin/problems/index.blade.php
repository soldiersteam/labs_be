@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Список задач</div>
                    <div class="col-sm-2">
                        <form method="post" enctype="multipart/form-data" data-problem-import-form
                              action="{{ action('Backend\ProblemController@importProblems') }}">
                            {{ csrf_field() }}
                            <label class="btn btn-primary btn-file">
                                Импортировать задачи <input data-problem-import type="file" name="problems[]" multiple/>
                            </label>
                        </form>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-primary" href="{{ action('Backend\ProblemController@showForm') }}">
                            Добавить задачу
                        </a>
                    </div>
                    <form method="get">
                        <div class="input-group col-sm-6">
                            <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control"
                                   placeholder="Текст для поиска...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Найти</button>
                                @if(Request::get('search'))
                                    <a class="btn btn-danger" href="{{ action('Backend\ProblemController@index') }}">Отменить</a>
                                @endif
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>archive</th>
                        <th>description</th>
                        <th>difficulty</th>
                        <th>deleted_at</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($problems as $problem)
                        <tr>
                            <td>{{ $problem->id }}</td>
                            <td>{{ $problem->name }}</td>
                            <td>{{ $problem->archive }}</td>
                            <td>{{ $problem->description }}</td>
                            <td>{{ $problem->difficulty  }}</td>
                            <td>{{ $problem->deleted_at }}</td>
                            <td>{{ $problem->created_at }}</td>
                            <td>{{ $problem->updated_at }}</td>
                            <td style="text-align: right; width: 187px">
                                <a class="btn btn-primary"
                                   href="{{ action('Backend\ProblemController@showForm', $problem->id) }}">Изменить</a>
                                <form method="post" style="display: inline"
                                      action="{{ action('Backend\ProblemController@delete', $problem->id) }}"
                                      class="delete_form_{{ $problem->id }}">
                                    {{ csrf_field() }}
                                    <a class="btn btn-danger" data-toggle="confirmation"
                                       data-btn-ok-href="javascript:$('.delete_form_{{ $problem->id }}').submit();">Удалить</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $problems->links() }}
            </div>
        </div>
    </div>
@endsection
