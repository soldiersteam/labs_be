@extends('layouts.admin')

@section('content')
    <div class="container-fluid-fluid-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Задачи в модуле №{{ Request::route('module_id') }}</div>
                    <div class="col-sm-5">
                        @if(!$problems_select->isEmpty())

                            <form method="post" action="{{ action('Backend\ModuleProblemController@add', [Request::route('id'), Request::route('module_id')]) }}">
                                {{ csrf_field() }}
                                <div class="col-sm-5">
                                    <select multiple name="problems[]" class="form-control tags_select2">
                                        @foreach($problems_select as $problem)
                                            <option value="{{ $problem->id }}">{{$problem->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary">Добавить задачи</button>
                                </div>
                            </form>
                        @endif

                    </div>
                    <div class="col-sm-4">
                        <form method="get">
                            <div class="input-group">
                                <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Найти</button>
                                    @if(Request::get('search'))
                                        <a class="btn btn-danger" href="{{ action('Backend\ModuleProblemController@index', [Request::route('id'), Request::route('module_id')]) }}">Отменить</a>
                                    @endif
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-1">

                        <a class="btn btn-primary" data-toggle="confirmation" data-btn-ok-href="javascript:$('.saveProblems').submit();" >Сохранить</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form method="post" action="{{ action('Backend\ModuleProblemController@edit', [Request::route('id'), Request::route('module_id')]) }}" class="saveProblems">
                    {{ csrf_field() }}
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>display_id</th>
                            <th style="width: 400px;">allowed languages</th>
                            <th>name</th>
                            <th>description</th>
                            <th>difficulty</th>
                            <th style="width:327px">actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($problems as $problem)
                            <tr>
                                <td>{{ $problem->id }}</td>
                                <td>
                                    <input class="form-control" name="display_ids[{{ $problem->id }}]" value="{{ $problem->pivot->display_id }}">
                                </td>
                                <td>
                                    <select class="form-control tags_select2" multiple name="allowed_languages[{{ $problem->id }}][]">
                                        @foreach($programming_languages as $language)
                                            <option value="{{ $language->id }}" {{$problem->programming_languages->contains($language) ? 'selected' : '' }}>{{$language->title}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>{{ $problem->name }}</td>
                                <td>{{ $problem->description }}</td>
                                <td>
                                    @for($i = 0; $i < $problem->difficulty; $i++)
                                        <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                                    @endfor
                                </td>
                                <td style="text-align: right; width: 200px">
                                    <form method="post" style="display: inline" action="{{ action('Backend\ModuleProblemController@delete', [Request::route('id'), Request::route('module_id'), $problem->id]) }}" class="delete_form_{{ $problem->id }}">
                                        {{ csrf_field() }}
                                        <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $problem->id }}').submit();" >Удалить</a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection