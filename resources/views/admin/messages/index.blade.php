@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-1">Сообщения</div>
                    <div class="col-sm-11">
                        <div class="col-sm-3">
                            <div class="conversation-wrapper">
                                <div class="list-group">
                                    @foreach( $conversations as $conversation)
                                        <a href="messages?page={{$conversations->currentPage()}}&conversation={{$conversation->id}}"
                                           class="list-group-item list-group-item-action {{$conversation->active}}">
                                            <img src="http://lorempixel.com/50/50/cats/       {{$conversation->name}}/"
                                                 class="img-circle"/>
                                            {{$conversation->name}}
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                            {{ $conversations->links() }}
                        </div>
                        <div class="col-sm-9">
                            <div class="message-wrapper">
                                @if(!sizeof($messages))
                                    <div class="alert alert-info">
                                        <strong>В диалоге нет сообщений!</strong>
                                    </div>
                                @else
                                    @foreach($messages as $message)
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="http://lorempixel.com/50/50/cats/       {{$message->user->name}}/"
                                                     class="media-object img-circle"" >
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <strong>{{$message->user->name}}</strong>
                                                    <small><i>{{$message->created_at}}</i></small>
                                                </h4>
                                                <p>{{$message->message}}</p>
                                                @if(count($message->attachements) !== 0)
                                                    <small><i><h6>Прикрепленные файлы:</h6></i></small>
                                                @endif
                                                @foreach($message->attachements as $attachment)
                                                    <a href="{{$attachment->path}}/{{$attachment->file_name}}.{{$attachment->extention}}"
                                                       class="btn btn-link">{{$attachment->file_name}}
                                                        .{{$attachment->extention}}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
