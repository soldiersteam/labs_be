@php
    /** @var $languages */
    /** @var \App\Support\LanguageService $languageService */
    /** @var \Illuminate\Support\ViewErrorBag $errors */
@endphp
@inject('languageService', '\App\Support\LanguageService')

<div class="container">
    <h3>Translatable</h3>
    <ul class="nav nav-tabs" data-tabs="tabs">
        @foreach($languages as  $language)
            @php /** @var \App\Support\FormLanguage $language */ @endphp
            <li class="{{ $errors->has('*.' . $language->id) ? 'has-error':'' }} {{ $loop->first ? 'active' :'' }}">
                <a data-toggle="tab" class="help-block " href="#{{ $language->id }}">{{ $language->name }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($languages as $language)
            @php /** @var \App\Support\FormLanguage $language */ @endphp
            <div id="{{ $language->id }}" class="tab-pane fade {{ $loop->first ? 'in active' : ''}} ">
                @foreach($language->attributes as $key => $attribute)
                    @php
                        /** @var \App\Support\TranslatableFormAttribute $attribute */
                        $value = old("{$key}.{$language->id}") ?: $languageService->getAttribute($model, $key, $language->id);
                        $formKey = $languageService->buildFormKey($key, $language->id);
                    @endphp

                    <div class="form-group{{ $errors->has($key . '.' . $language->id) ? ' has-error' : '' }}">
                        <label for="{{ $formKey }}"
                               class="col-md-4 control-label">{{ $attribute->name }}</label>
                        <div class="col-md-6">
                            @if($attribute->isText())
                                <input id="{{ $formKey }}"
                                       class="form-control"
                                       name="{{$formKey }}"
                                       value="{{ $value }}">
                            @elseif($attribute->isTextArea())
                                <textarea id="{{ $formKey }}"
                                          class="form-control"
                                          name="{{ $formKey }}"
                                >{{ $value }}</textarea>
                            @endif
                            @if ($errors->has($key . '.' . $language->id))
                                <span class="help-block">
                                    <strong>{{ $errors->first($key . '.' . $language->id) }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
</div>
