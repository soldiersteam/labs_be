@extends('layouts.admin')

@section('content')
<div class="container-fluid-fluid">
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-2">Курсы</div>
                <div class="col-sm-3"><a class="btn btn-primary" href="{{ action('Backend\CourseController@showForm') }}">Добавить Курс</a></div>

                <form method="get">
                    <div class="input-group col-sm-6">
                        <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Найти</button>
                            @if(Request::get('search'))
                                <a class="btn btn-danger" href="{{ action('Backend\CourseController@index') }}">Отменить</a>
                            @endif
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>description</th>
                    <th>content</th>
                    <th>duration</th>
                    <th>price UAH</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)
                    <tr>
                        <td>{{ $course->id }}</td>
                        <td>{{ $course->name }}</td>
                        <td>{{ $course->description }}</td>
                        <td>{{ $course->content }}</td>
                        <td>{{ $course->duration }}</td>
                        <td>{{ $course->price }}</td>
                        <td style="text-align: right; width: 268px">
                            <a class="btn btn-default" href="{{ action('Backend\ModuleController@index', $course->id) }}">Модули</a>
                            <a class="btn btn-primary" href="{{ action('Backend\CourseController@showForm', $course->id) }}">Изменить</a>
                            <form method="post" style="display: inline" action="{{ action('Backend\CourseController@delete', $course->id) }}" class="delete_form_{{ $course->id }}">
                                {{ csrf_field() }}
                                <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $course->id }}').submit();" >Удалить</a>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $courses->links() }}
        </div>
    </div>
</div>
@endsection

