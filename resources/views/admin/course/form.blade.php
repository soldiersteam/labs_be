@extends('layouts.admin')

@section('content')
    <div class="container-fluid-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $course->exists ? 'Изменить ' . $course->name : 'Создать Курс' }}</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST"
                      action="{{ $course->exists ? action('Backend\CourseController@update', $course->id) : action('Backend\CourseController@create') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                        <label for="duration" class="col-md-4 control-label">Duration</label>

                        <div class="col-md-6">
                            <input id="duration" type="number" class="form-control" name="duration"
                                   value="{{ old('duration') ?: $course->duration }}">

                            @if ($errors->has('duration'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('duration') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label for="price" class="col-md-4 control-label">Price in $</label>

                        <div class="col-md-6">
                            <input id="price" type="number" class="form-control" name="price" step="0.01"
                                   value="{{ old('price') ?: $course->priceUSD }}">
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        @include('admin.translation-tabs', ['languages' => $languages, 'model' => $course])
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $course->exists ? 'Изменить Курс' : 'Создать Курс' }}
                            </button>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4 ">
                            <a href="{{ action('Backend\CourseController@index') }}" class="btn btn-primary pull-right">
                                {{ 'Назад' }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
