@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">Языки</div>
                    <div class="col-sm-3"><a class="btn btn-primary" href="{{ action('Backend\LanguageController@showForm') }}">Добавить Язык</a></div>

                    <form method="get">
                        <div class="input-group col-sm-6">
                            <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Текст для поиска...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Найти</button>
                                @if(Request::get('search'))
                                    <a class="btn btn-danger" href="{{ action('Backend\LanguageController@index') }}">Отменить</a>
                                @endif
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>code</th>
                        <th>actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $language)
                        <tr>
                            <td>{{ $language->id }}</td>
                            <td>{{ $language->name }}</td>
                            <td>{{ $language->code }}</td>
                            <td style="text-align: right; width: 800px">

                                <a class="btn btn-default" href="{{ action('Backend\LanguageController@exportTranslations', $language->id) }}">Экспорт</a>
                                <a class="btn btn-primary" href="{{ action('Backend\LanguageController@showForm', $language->id) }}">Изменить</a>
                                <form method="post" style="display: inline" action="{{ action('Backend\LanguageController@delete', $language->id) }}" class="delete_form_{{ $language->id }}">
                                    {{ csrf_field() }}
                                    <a class="btn btn-danger" data-toggle="confirmation" data-btn-ok-href="javascript:$('.delete_form_{{ $language->id }}').submit();" >Удалить</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $languages->links() }}
            </div>
        </div>
    </div>
@endsection
