@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">{{ $language ? 'Изменить Язык № ' . $language->id : 'Добавить Язык' }}</div>
            <div class="panel-body">
                <form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="{{ $language ? action('Backend\LanguageController@update', $language->id) : action('Backend\LanguageController@create') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ?: ($language ? $language->name :'') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code" class="col-md-4 control-label">Code</label>

                        <div class="col-md-6">
                            <input id="code" type="text" class="form-control" name="code" value="{{ old('code') ?: ($language ? $language->code :'') }}" required autofocus>

                            @if ($errors->has('code'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="translations_json" class="col-md-4 control-label">Переводы</label>

                        <div class="col-md-6">
                            <input id="translations_json" type="file" class="form-control" name="translations_json">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                {{ $language ? 'Изменить Язык' : 'Добавить Язык' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
