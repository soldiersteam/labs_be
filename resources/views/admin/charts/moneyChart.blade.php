<div class="panel">
    <div class="panel-heading">$ Chart</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group">
                <label for="from" class="col-md-1 control-label">From</label>

                <div class="col-md-5">
                    <input type="date" id="from" name="from" class="form-control moneyChartFrom">
                </div>
                <label for="to" class="col-md-1 control-label">To</label>

                <div class="col-md-5">
                    <input type="date" id="to" name="to" class="form-control moneyChartTo">
                </div>
            </div>
            <div class="col-sm-12 moneyChartContainer">
                <canvas id="moneyChart"></canvas>
            </div>
        </div>
    </div>
</div>