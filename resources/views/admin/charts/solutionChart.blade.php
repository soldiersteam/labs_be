<div class="panel">
    <div class="panel-heading">Solution chart</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group">
                <label for="from" class="col-md-1 control-label">From</label>

                <div class="col-md-5">
                    <input type="date" id="from" name="from" class="form-control solutionChartFrom">
                </div>
                <label for="to" class="col-md-1 control-label">To</label>

                <div class="col-md-5">
                    <input type="date" id="to" name="to" class="form-control solutionChartTo">
                </div>
            </div>
            <div class="col-sm-12 solutionChartContainer">
                <canvas id="solutionChart"></canvas>
            </div>
        </div>
    </div>
</div>