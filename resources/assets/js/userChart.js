(($, window, document) => {
    $(document).ready(() => {
        if($("#userChart").length) {
            init();
            refresh();
        }

        function init() {

            let from = moment().subtract(1, 'months')
            let to = moment();

            $('.userChartFrom').val(from.format('YYYY-MM-DD'));
            $('.userChartTo').val(to.format('YYYY-MM-DD'));

            $('.userChartFrom').change(() => refresh());
            $('.userChartTo').change(() => refresh());
        }

        function refresh() {

            $.ajax(`/ajax/dashboard/user-chart?from=${$('.userChartFrom').val()}&to=${$('.userChartTo').val()}`)
                .done((data) => {
                    renderChart(data)
                });
        }

        function renderChart(data) {

            var oldCanvas = $("#userChart")[0].outerHTML;
            $(".userChartContainer").empty();
            $(".userChartContainer").append(oldCanvas);

            var ctx = document.getElementById("userChart").getContext('2d');
            var userChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: data.map(el => el.created_at),
                    datasets: [{
                        label: 'Amount of registered users',
                        data: data.map(el => el.id)
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            }
                        }],

                        xAxes: [{
                            type: 'time',
                        }]
                    }
                }
            });
        }
    });
})(jQuery, window, document);