

require('./bootstrap');

require('./main');
require('./modal');
require('./problem');
require('./moneyChart');
require('./userChart');
require('./solutionChart');
require('./makeRequest');