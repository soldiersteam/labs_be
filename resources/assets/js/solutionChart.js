(($, window, document) => {
    $(document).ready(() => {

        if($("#userChart").length) {
            init();
            refresh();
        }

        function init() {

            let from = moment().subtract(1, 'months')
            let to = moment();

            $('.solutionChartFrom').val(from.format('YYYY-MM-DD'));
            $('.solutionChartTo').val(to.format('YYYY-MM-DD'));

            $('.solutionChartFrom').change(() => refresh());
            $('.solutionChartTo').change(() => refresh());
        }

        function refresh() {

            $.ajax(`/ajax/dashboard/solution-chart?from=${$('.solutionChartFrom').val()}&to=${$('.solutionChartTo').val()}`)
                .done((data) => {
                    renderChart(data)
                });
        }

        function renderChart(data) {

            var oldCanvas = $("#solutionChart")[0].outerHTML;
            $(".solutionChartContainer").empty();
            $(".solutionChartContainer").append(oldCanvas);

            var ctx = document.getElementById("solutionChart").getContext('2d');
            var solutionChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: data.map(el => el.created_at),
                    datasets: [{
                        label: 'Amount of submitted solutions',
                        data: data.map(el => el.id)
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            }
                        }],

                        xAxes: [{
                            type: 'time',
                        }]
                    }
                }
            });
        }
    });
})(jQuery, window, document);