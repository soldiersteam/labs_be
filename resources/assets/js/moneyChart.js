(($, window, document) => {
    $(document).ready(() => {

        if($("#userChart").length) {
            init();
            refresh();
        }

        function init() {

            let from = moment().subtract(1, 'months')
            let to = moment();

            $('.moneyChartFrom').val(from.format('YYYY-MM-DD'));
            $('.moneyChartTo').val(to.format('YYYY-MM-DD'));

            $('.moneyChartFrom').change(() => refresh());
            $('.moneyChartTo').change(() => refresh());
        }

        function refresh() {

            $.ajax(`/ajax/dashboard/money-chart?from=${$('.moneyChartFrom').val()}&to=${$('.moneyChartTo').val()}`)
                .done((data) => {
                    renderChart(data)
                });
        }

        function renderChart(data) {

            var oldCanvas = $("#moneyChart")[0].outerHTML;
            $(".moneyChartContainer").empty();
            $(".moneyChartContainer").append(oldCanvas);

            var ctx = document.getElementById("moneyChart").getContext('2d');
            var moneyChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: data.map(el => el.updated_at),
                    datasets: [{
                        label: 'Amount of income',
                        data: data.map(el => el.amount)
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{//money
                            ticks: {
                                beginAtZero: true,
                            }
                        }],

                        xAxes: [{
                            type: 'time',
                        }]
                    }
                }
            });
        }
    });
})(jQuery, window, document);