<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/12/17
 * Time: 4:25 PM
 */

/*Broadcast::channel('conversations.{conversation_id}', function ($user, $conversationId) {
    return app(\App\Support\ConversationService::class)->userHas($user->id, $conversationId) ?
        $user->name : null;
});*/

Broadcast::channel('user.{user_id}', function ($user, $user_id) {
    Log::info(print_r($user));
    Log::info(print_r($user_id));
    return $user->id == $user_id;
});