<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'access:web,0,' . App\User::ROLE_ADMIN], function () {
    Route::group(['prefix' => 'ajax'], function() {
        Route::get('/dashboard/money-chart', 'Backend\DashboardController@moneyChartData');
        Route::get('/dashboard/solution-chart', 'Backend\DashboardController@solutionChartData');
        Route::get('/dashboard/user-chart', 'Backend\DashboardController@userChartData');
    });

    Route::get('/', 'Backend\DashboardController@index');
    Route::group(['prefix' => 'servers'], function() {
        Route::get('/', 'Backend\ServerController@index');
        Route::get('/form/{id?}', 'Backend\ServerController@showForm');
        Route::post('/{id}', 'Backend\ServerController@update');
        Route::post('/', 'Backend\ServerController@create');
        Route::post('/delete/{id}', 'Backend\ServerController@delete');
    });
    Route::group(['prefix' => 'courses'], function () {
        Route::get('/', 'Backend\CourseController@index');
        Route::get('/form/{id?}', 'Backend\CourseController@showForm');
        Route::post('/{id}', 'Backend\CourseController@update');
        Route::post('/', 'Backend\CourseController@create');
        Route::post('/delete/{id}', 'Backend\CourseController@delete');
    });
    Route::group(['prefix' => 'problems'], function() {
        Route::post('/import', 'Backend\ProblemController@importProblems');
        Route::get('/', 'Backend\ProblemController@index');
        Route::get('/form/{id?}', 'Backend\ProblemController@showForm');
        Route::post('/{id}', 'Backend\ProblemController@update');
        Route::post('/', 'Backend\ProblemController@create');
        Route::post('/delete/{id}', 'Backend\ProblemController@delete');
    });
    Route::group(['prefix' => 'users'], function() {
        Route::get('/', 'Backend\UserController@index');
        Route::get('/form/{id?}', 'Backend\UserController@showForm');
        Route::post('/{id}', 'Backend\UserController@update');
        Route::post('/', 'Backend\UserController@create');
        Route::post('/delete/{id}', 'Backend\UserController@delete');
    });
    Route::group(['prefix' => 'solutions'], function() {
        Route::get('/', 'Backend\SolutionController@index');
        Route::get('/form/{id?}', 'Backend\SolutionController@showForm');
        Route::post('/{id}', 'Backend\SolutionController@update');
        Route::post('/', 'Backend\SolutionController@create');
        Route::post('/delete/{id}', 'Backend\SolutionController@delete');
    });
    Route::group(['prefix' => 'messages'], function() {
        Route::get('/', 'Backend\MessageController@index');
    });
    Route::group(['prefix' => 'programming-languages'], function() {
        Route::get('/', 'Backend\ProgrammingLanguageController@index');
        Route::get('/form/{id?}', 'Backend\ProgrammingLanguageController@showForm');
        Route::post('/{id}', 'Backend\ProgrammingLanguageController@update');
        Route::post('/', 'Backend\ProgrammingLanguageController@create');
        Route::post('/delete/{id}', 'Backend\ProgrammingLanguageController@delete');
    });
    Route::group(['prefix' => 'languages'], function() {
        Route::get('/', 'Backend\LanguageController@index');
        Route::get('/form/{id?}', 'Backend\LanguageController@showForm');
        Route::post('/{id}', 'Backend\LanguageController@update');
        Route::post('/', 'Backend\LanguageController@create');
        Route::post('/delete/{id}', 'Backend\LanguageController@delete');
        Route::get('/translations/{id}', 'Backend\LanguageController@translations');
        Route::get('/translations/{id}/export', 'Backend\LanguageController@exportTranslations');
    });

    Route::group(['prefix' => 'transactions'], function() {
        Route::get('/', 'Backend\TransactionController@index');
    });
    Route::group(['prefix' => 'courses'], function() {
        Route::get('/', 'Backend\CourseController@index');
        Route::get('/form/{id?}', 'Backend\CourseController@showForm');
        Route::post('/{id}', 'Backend\CourseController@update');
        Route::post('/', 'Backend\CourseController@create');
        Route::post('/delete/{id}', 'Backend\CourseController@delete');

        Route::group(['prefix' => '{id}/modules'], function() {
            Route::get('/', 'Backend\ModuleController@index');
            Route::get('/form/{module_id?}', 'Backend\ModuleController@showForm');
            Route::post('/{module_id}', 'Backend\ModuleController@update');
            Route::post('/', 'Backend\ModuleController@create');
            Route::post('/delete/{module_id}', 'Backend\ModuleController@delete');

            Route::group(['prefix' => '{module_id}/problems'], function() {
                Route::get('/', 'Backend\ModuleProblemController@index');
                Route::get('/form/{problem_id?}', 'Backend\ModuleProblemController@showForm');
                Route::post('/', 'Backend\ModuleProblemController@add');
                Route::post('/edit', 'Backend\ModuleProblemController@edit');
                Route::post('/delete/{problem_id}', 'Backend\ModuleProblemController@delete');
            });
        });
    });
});

Route::group(['middleware' => 'guest'], function () {
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
});

$this->post('logout', 'Auth\LoginController@logout')->name('logout');