<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\User;

Route::group(['namespace' => 'Api', 'prefix' => 'testing-system-api'], function () {
    Route::post('clients/auth/', 'ClientController@getToken')->middleware('auth.api_custom:client_login,true');
    Route::post('auth/token', 'TestingServerController@getToken')->middleware('auth.api_custom:server_login,true');

    Route::group(['middleware' => 'auth.api_custom:server_auth'], function () {
        Route::get('problems/{id}/tests.tar.gz', 'ProblemController@getArchive');
        Route::group(['prefix' => 'solutions'], function () {
            Route::patch('{id}', 'SolutionController@update')->where('id', '[0-9]+');
            Route::get('{id}/source-code', 'SolutionController@show_source_code')->where('id', '[0-9]+');
            Route::patch('latest-new', 'SolutionController@latest_new');
            Route::post('{id}/testing-report', 'SolutionController@store_report')->where('id', '[0-9]+');
        });
        Route::get('programming-languages', 'ProgrammingLanguageController@index');
    });
});

Route::group(['namespace' => 'App', 'middleware' => 'set_locale'], function () {
    Route::get('languages', 'LanguageController@index');
    Route::get('translations', 'LanguageController@translations');
    Route::get('programming-languages', 'ProgrammingLanguageController@index');

    Route::group(['namespace' => 'Auth'], function () {
        Route::post('register', 'RegisterController@register');
        Route::post('validate-email', 'RegisterController@validateEmail');
        Route::post('resend-validation-email', 'RegisterController@resend');


        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
        Route::post('password/reset', 'ResetPasswordController@reset');
    });
    Route::group(['prefix' => 'users'], function () {
        Route::post('auth', 'UserController@getToken')->middleware('auth.api_custom:user_login,true');
        Route::group(['middleware' => 'auth.api_custom:user_auth'], function () {
            Route::post('auth/logout', 'UserController@logout');
        });
    });

    Route::group(['middleware' => 'auth.api_custom:user_auth'], function () {
        Route::group(['prefix' => 'solutions'], function () {
            Route::get('/', 'SolutionController@index');
            Route::get('{id}', 'SolutionController@single');
            Route::get('{id}/reports', 'SolutionController@reports');
            Route::post('{id}/discuss', 'SolutionController@discuss');
            Route::post('/', 'SolutionController@create')->middleware('course_check:module_id');
        });

        Route::group([], function () {
            Route::group(['prefix' => 'problems', 'middleware' => 'course_check:module_id'], function () {
                Route::get('/', 'ProblemController@index');
                Route::get('/{id}', 'ProblemController@single');
            });
            Route::post('problems/{id}/discuss', 'ProblemController@discuss');

            Route::group(['prefix' => 'modules'], function () {
                Route::get('/', 'ModuleController@index')->middleware('course_check:course_id');
                Route::get('{id}', 'ModuleController@single');
            });
            Route::get('courses/{id}', 'CourseController@show')->middleware('course_check');
        });


        Route::get('programming-languages/{id}', 'ProgrammingLanguageController@show');
        Route::get('users/{id}', 'UserController@read');
        Route::post('users/{user_id}', 'UserController@update')->middleware('can_update_user');

        Route::group(['prefix' => 'curator', 'namespace' => 'Curator', 'middleware' => 'access:user_auth,0,' . User::ROLE_CURATOR], function () {
            Route::group(['prefix' => 'users'], function () {
                Route::get('', 'UserController@index');
                Route::get('search', 'UserController@search');
                Route::get('{id}', 'UserController@show');
                Route::post('{id}/give-access', 'UserController@giveAccess');

                Route::post('{userId}/notes', 'UserNoteController@store');
                Route::get('{userId}/notes', 'UserNoteController@index');
            });
            Route::delete('notes/{id}', 'UserNoteController@delete');


            Route::get('interviews', 'InterviewController@index');
            Route::post('interviews/{id}/complete', 'InterviewController@complete');
            Route::get('solutions', 'SolutionController@index');
            Route::post('solutions/comment', 'CommentController@comment');
            Route::get('solution-groups', 'SolutionController@groupIndex');
            Route::get('courses', 'CourseController@index');
            Route::get('courses/search', 'CourseController@search');
            Route::get('problems', 'ProblemController@index');
            Route::get('problems/{id}', 'ProblemController@show');
            Route::get('programming-languages/search', 'ProgrammingLanguageController@search');
        });

        Route::group(['prefix' => 'parent', 'namespace' => 'Parent', 'middleware' => 'access:user_auth,0,' . User::ROLE_PARENT], function () {
            Route::post('register', 'RegisterController@register');
            Route::get('children', 'ChildController@index');
            Route::post('children/{id}/schedule', 'ChildController@schedule');
            Route::group(['prefix' => 'courses'], function () {
                Route::get('/', 'CourseController@index');
                Route::get('{id}', 'CourseController@single');
                Route::get('{id}/buy', 'CourseController@getBuyData');
            });
        });

        Route::group(['prefix' => 'student', 'namespace' => 'Student', 'middleware' => 'access:user_auth,0,' . User::ROLE_USER], function () {
            Route::get('owned-courses', 'CourseController@getOwned');
        });

        Route::group(['prefix' => 'conversations'], function () {
            Route::get('', 'ConversationController@index');
            Route::post('', 'ConversationController@writeToUser');
            Route::post('discuss', 'ConversationController@discuss');
            Route::get('{id}', 'ConversationController@show');
            Route::post('{id}', 'ConversationController@send');
            Route::post('{id}/read', 'ConversationController@read');
            Route::get('{id}/messages', 'ConversationController@getMessages');
        });

        Route::get('messages/unread', 'MessageController@unread');
    });

    Route::group(['prefix' => 'courses', 'middleware' => 'auth.set_guard:user_auth'], function () {
    });
    Route::post('invite-parent', 'UserController@inviteParent');
});

Route::group(['prefix' => 'transaction'], function () {
    Route::any('callback', 'TransactionController@callback');
});
