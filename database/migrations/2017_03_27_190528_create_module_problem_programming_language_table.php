<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleProblemProgrammingLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_problem_programming_language', function (Blueprint $table) {
            $table->unsignedInteger('module_id');
            $table->unsignedInteger('problem_id');
            $table->unsignedInteger('programming_language_id');

            $table->foreign('module_id', 'module_foreign')
                ->references('id')->on('modules')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('problem_id', 'problem_foreign')
                ->references('id')->on('problems')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('programming_language_id', 'programming_language_foreign')
                ->references('id')->on('programming_languages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_problem_programming_language');
    }
}
