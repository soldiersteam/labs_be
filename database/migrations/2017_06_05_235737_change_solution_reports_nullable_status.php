<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSolutionReportsNullableStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solution_reports', function (Blueprint $table) {
            \DB::statement("ALTER TABLE solution_reports MODIFY status ENUM('CE', 'FF', 'NC', 'CC', 'CT', 'UE', 'OK', 'WA', 'PE', 'RE', 'TL', 'ML')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solution_reports', function (Blueprint $table) {
            \DB::statement("ALTER TABLE solution_reports MODIFY status ENUM('CE', 'FF', 'NC', 'CC', 'CT', 'UE', 'OK', 'WA', 'PE', 'RE', 'TL', 'ML') NOT NULL");
        });
    }
}
