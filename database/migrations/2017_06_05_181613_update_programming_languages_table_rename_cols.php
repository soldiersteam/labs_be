<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProgrammingLanguagesTableRenameCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programming_languages', function (Blueprint $table) {
            $table->renameColumn('compiler_image', 'compiler_docker_image_name');
            $table->renameColumn('executor_image', 'executor_docker_image_name');
            $table->renameColumn('name', 'title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programming_languages', function (Blueprint $table) {

            $table->renameColumn('compiler_docker_image_name', 'compiler_image');
            $table->renameColumn('executor_docker_image_name', 'executor_image');
            $table->renameColumn('title', 'name');
        });
    }
}
