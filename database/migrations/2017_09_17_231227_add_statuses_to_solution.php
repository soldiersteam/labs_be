<?php

use App\Status;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusesToSolution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solutions', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 3);
        });
        Status::create(['code' => 'CE']);
        Status::create(['code' => 'FF']);
        Status::create(['code' => 'NC']);
        Status::create(['code' => 'CC']);
        Status::create(['code' => 'CT']);
        Status::create(['code' => 'UE']);
        Status::create(['code' => 'OK']);
        Status::create(['code' => 'WA']);
        Status::create(['code' => 'PE']);
        Status::create(['code' => 'RE']);
        Status::create(['code' => 'TL']);
        Status::create(['code' => 'ML']);
        Status::create(['code' => 'ZR']);

        Schema::create('solution_status', function (Blueprint $table) {
            $table->unsignedBigInteger('solution_id');
            $table->unsignedInteger('status_id');

            $table->foreign('solution_id')
                ->references('id')->on('solutions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('status_id')
                ->references('id')->on('statuses')
                ->onDelete('cascade')
                ->onUpdate('cascade');


            $table->unique(['solution_id', 'status_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solutions', function (Blueprint $table) {
            $table->enum('status', ['OK', 'CE', 'FF', 'NC', 'CC', 'CT', 'UE', 'ZR'])->nullable();
        });

        Schema::drop('solution_status');
        Schema::drop('statuses');
    }
}
