<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => 'secret',
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Solution::class, function (Faker\Generator $faker) {
    $module = \App\Module::inRandomOrder()->has('problems')->first();
    $problem = $module->problems()->withProgrammingLanguages($module->id)->inRandomOrder()->first();
    return [
        'state'                   => $faker->randomElement(\App\Solution::getStates()),
        'success_percentage'      => $faker->numberBetween(0, 100),
        'message'                 => 'ok',
        'user_id'                 => \App\User::whereRole(\App\User::ROLE_USER)->inRandomOrder()->first()->id,
        'module_id'               => $module->id,
        'course_id'               => $module->course_id,
        'problem_id'              => $problem->id,
        'programming_language_id' => \App\ProgrammingLanguage::inRandomOrder()->first()->id
    ];
});
