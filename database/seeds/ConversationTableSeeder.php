<?php

use App\Conversation;
use Illuminate\Database\Seeder;

class ConversationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Conversation::create([
            'name'     => 'Conf 1'
        ]);
        Conversation::create([
            'name'     => 'Conf 2'
        ]);
        Conversation::create([
            'name'     => 'Conf 3'
        ]);
        Conversation::create([
            'name'     => 'Conf 4'
        ]);
        Conversation::create([
            'name'     => 'Conf 5'
        ]);
        Conversation::create([
            'name'     => 'Conf 6'
        ]);
        Conversation::create([
            'name'     => 'Conf 7'
        ]);
        Conversation::create([
            'name'     => 'Conf 8'
        ]);
        Conversation::create([
            'name'     => 'Conf 9'
        ]);
        Conversation::create([
            'name'     => 'Conf 10'
        ]);
        Conversation::create([
            'name'     => 'Conf 11'
        ]);
        Conversation::create([
            'name'     => 'Conf 12'
        ]);
        Conversation::create([
            'name'     => 'Conf 13'
        ]);
        Conversation::create([
            'name'     => 'Conf 14'
        ]);
        Conversation::create([
            'name'     => 'Conf 15'
        ]);
    }
}
