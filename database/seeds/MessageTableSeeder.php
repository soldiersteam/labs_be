<?php

use App\Message;
use Illuminate\Database\Seeder;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::create([
            'message'     => 'hi',
            'is_read'    => '0',
            'conversation_id' => '1',
            'user_id'     => '1'
        ]);
        Message::create([
            'message'     => 'lorem',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '2'
        ]);
        Message::create([
            'message'     => 'hi admin',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '2'
        ]);
        Message::create([
            'message'     => 'hi admin2',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '1'
        ]);

        Message::create([
            'message'     => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et officiis eius quod incidunt, esse, non ad velit at fugiat similique, laboriosam magnam vero unde tempore. Iure quidem reprehenderit, possimus veniam.',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '1'
        ]);
        Message::create([
            'message'     => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quis doloremque quo dolore labore unde! Possimus fugiat a doloribus, similique at facilis aspernatur architecto, tempore omnis voluptatum saepe est reprehenderit sequi ipsum maxime placeat consequuntur ut ipsam asperiores sed, voluptatibus eveniet autem alias vitae. Dolorem laboriosam maiores vitae, laudantium possimus.',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '1'
        ]);
        Message::create([
            'message'     => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt harum perspiciatis natus deserunt, sequi tempora consequatur explicabo nobis aliquam! Sapiente tempora nesciunt optio quia, sunt, non consectetur repudiandae consequatur nihil dolore quo ipsa, facilis earum ea aspernatur velit nostrum ipsam! Reiciendis dolore accusantium quas quisquam provident praesentium, itaque qui distinctio vero animi tempore at consequuntur beatae magnam nulla amet, quasi velit veritatis repudiandae, dolores earum. Sequi magnam fugit unde. Soluta temporibus maxime qui veniam amet consequatur quia autem saepe magnam vitae modi repellat quaerat, dolor eveniet, laboriosam praesentium suscipit sapiente quibusdam nostrum consequuntur ut quisquam architecto voluptatibus. Quasi, sunt eligendi.',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '2'
        ]);
        Message::create([
            'message'     => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, excepturi?',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '1'
        ]);
        Message::create([
            'message'     => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis tempora omnis adipisci blanditiis unde sed, quos iste deserunt minima, natus ratione placeat cumque quisquam. Ex nisi facere possimus voluptatum cumque aliquid, non. Incidunt nisi saepe reiciendis voluptates velit neque, inventore voluptas quasi. Quod optio praesentium exercitationem vitae veritatis ratione, ipsam hic omnis nulla minus. Aut maxime magni ea fugit deserunt.',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '2'
        ]);
         Message::create([
            'message'     => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint ipsum aspernatur perferendis beatae facilis temporibus animi laborum sit, et quibusdam dolorum reiciendis, esse natus. Est facilis, repellendus at minus sit. Voluptatum voluptas consequatur eos nobis. Deleniti sit adipisci, beatae impedit, ratione accusantium architecto temporibus ab sapiente expedita. Error dicta quos et porro reiciendis omnis. Quia eos est, eveniet sed placeat beatae id iste, consequatur, suscipit consequuntur similique quaerat earum, deleniti odit fuga vel. Perferendis nihil magnam qui adipisci quos incidunt sint, officiis, unde impedit temporibus aliquid blanditiis, quis repudiandae id? Itaque expedita at earum doloribus quasi exercitationem placeat possimus veniam.',
            'is_read'    => '0',
            'conversation_id' => '2',
            'user_id'     => '2'
        ]);
    }
}
