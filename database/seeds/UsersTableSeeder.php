<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'admin',
            'email'    => 'admin@admin.com',
            'password' => '123123',
            'role'     => User::ROLE_ADMIN,
            'api_token' => ''
        ]);
        User::create([
            'name'     => 'admin2',
            'email'    => 'admin2@admin.com',
            'password' => '123123',
            'role'     => User::ROLE_ADMIN,
            'api_token' => ''
        ]);
    }
}
