<?php

use App\MessageAttachment;
use Illuminate\Database\Seeder;

class MessageAttachmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MessageAttachment::create([
            'message_id'     => '2',
            'path'    => '/path',
            'size' => '123',
            'extension'     => 'doc',
            'file_name' => 'document'
        ]);
        MessageAttachment::create([
            'message_id'     => '3',
            'path'    => '/path',
            'size' => '123',
            'extension'     => 'docx',
            'file_name' => 'doc'
        ]);
        MessageAttachment::create([
            'message_id'     => '8',
            'path'    => '/path',
            'size' => '123',
            'extension'     => 'cpp',
            'file_name' => 'main'
        ]);
    }
}
