<?php

use Illuminate\Database\Seeder;

class ProgrammingLangsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'C (C89)', 'ace_mode' => 'c_cpp']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'C (C11)', 'ace_mode' => 'c_cpp']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'C++ (C++03)', 'ace_mode' => 'c_cpp']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'C++ (C++11)', 'ace_mode' => 'c_cpp']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'C++ (C++14)', 'ace_mode' => 'c_cpp']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Pascal', 'ace_mode' => 'pascal']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Delphi', 'ace_mode' => '']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Java 7', 'ace_mode' => 'java']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Java 8', 'ace_mode' => 'java']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Scala', 'ace_mode' => 'scala']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Kotlin', 'ace_mode' => 'kotlin']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Go', 'ace_mode' => 'golang']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Haskell', 'ace_mode' => 'haskell']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Nim', 'ace_mode' => '']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Rust', 'ace_mode' => 'rust']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Python 2', 'ace_mode' => 'python']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Python 3', 'ace_mode' => 'python']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Ruby', 'ace_mode' => 'ruby']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'PHP 5.6', 'ace_mode' => 'php']);
        \App\ProgrammingLanguage::create(['compiler_docker_image_name' => '', 'executor_docker_image_name' => '','title' => 'Bash 4.3', 'ace_mode' => '']);
    }
}
