FROM nginx:latest
COPY . /app
WORKDIR /app
RUN apt-get update && \
    apt-get install -y build-essential curl gnupg && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs && \
    npm i && \
    npm rebuild node-sass && \
    npm run prod