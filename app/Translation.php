<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Translation
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property int $translatable_id
 * @property string $translatable_type
 * @property int $language_id
 * @property-read \App\Language $languages
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $translatable
 * @method static \Illuminate\Database\Query\Builder|\App\Translation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Translation whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Translation whereLanguageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Translation whereTranslatableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Translation whereTranslatableType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Translation whereValue($value)
 * @mixin \Eloquent
 */
class Translation extends Model
{

    protected $visible = ['key', 'value'];

    protected $fillable = [
        'key',
        'value',
        'language_id',
    ];

    public $timestamps = false;

    public function languages() {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function translatable()
    {
        return $this->morphTo();
    }
}
