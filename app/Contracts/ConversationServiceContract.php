<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 6:56 PM
 */

namespace App\Contracts;


use Illuminate\Database\Eloquent\Model;

interface ConversationServiceContract
{
    public function send($fromUserId, $toConversationId, $message);

    public function createConversation($fromUserId, $toUserId = null);

    public function getForUser($id, $userId);

    public function markConversationAsReadForUser($conversationId, $userId);

    public function getMessages($id, $userId, $beforeId = null, $perPage = 20);

    public function writeToUser($fromId, $message, $toId);

    public function discuss($fromId, $message, Model $topic = null);

    public function getUserConversations($userId, $courseId = null, $perPage);

    public function getPendingConversations($userId, $courseId = null, $perPage);
}