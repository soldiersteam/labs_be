<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 7:38 PM
 */

namespace App\Contracts;


use App\Support\Conversations\ConversationHelperService;
use App\User;

interface ConversationRestrictionStateContract
{
    public function __construct(User $user, ConversationHelperService $conversationService);

    public function verifySend($fromUserId, $toConversationId, $message);

    public function verifyDiscuss($fromUserId, $toUserId, $topic = null);

    public function verifyGetForUser($id, $userId);

    public function verifyMarkConversationAsReadForUser($conversationId, $userId);

    public function verifyGetMessages($id, $userId, $beforeId = null);

    public function verifyWriteToUser($fromId, $toId, $message);

    public function verifyGetUserConversations($userId, $courseId);

    public function verifyGetPendingConversations($userId, $courseId);
}