<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ModuleProblemProgrammingLanguage
 *
 * @property int $module_id
 * @property int $problem_id
 * @property int $programming_language_id
 * @property-read \App\Module $modules
 * @property-read \App\Problem $problems
 * @property-read \App\ProgrammingLanguage $programming_languages
 * @method static \Illuminate\Database\Query\Builder|\App\ModuleProblemProgrammingLanguage whereModuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ModuleProblemProgrammingLanguage whereProblemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ModuleProblemProgrammingLanguage whereProgrammingLanguageId($value)
 * @mixin \Eloquent
 */
class ModuleProblemProgrammingLanguage extends Model
{
    protected $table = 'module_problem_programming_language';

    public $timestamps = false;

    protected $fillable = ['module_id', 'problem_id', 'programming_language_id'];

    public function modules() {
        return $this->belongsTo(Module::class);
    }
    public function problems() {
        return $this->belongsTo(Problem::class);
    }

    public function programming_languages()
    {
        return $this->belongsTo(ProgrammingLanguage::class);
    }
}
