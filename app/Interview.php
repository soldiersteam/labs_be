<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Interview
 *
 * @property int $id
 * @property int $curator_id
 * @property int $student_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property bool $completed
 * @property-read \App\User $curator
 * @property-read \App\User $student
 * @method static \Illuminate\Database\Query\Builder|\App\Interview whereCompleted($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Interview whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Interview whereCuratorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Interview whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Interview whereStudentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Interview whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Interview extends Model
{
    public function curator()
    {
        return $this->belongsTo(User::class, 'curator_id');
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id');
    }
}
