<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolutionGroup extends Model
{
    use SolutionRelations;

    const ELEMENTS = ['user_id', 'module_id', 'problem_id', 'comment_id', 'programming_language_id', 'course_id'];
    const SELECT = ['problems.name', 'programming_languages.title', 'courses.name', 'users.name', 'comments.text'];

    protected $table = 'solutions';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->visible = array_merge(self::ELEMENTS, array_map(function ($el) {
            return self::getAttrName($el);
        }, self::SELECT));
    }

    public static function getSelects()
    {
        return array_merge(self::getDistinctElements(), array_map(function ($el) {
            return $el . ' AS ' . self::getAttrName($el);
        }, self::SELECT));
    }

    public static function getAttrName($attr)
    {
        return str_replace('.', '_', $attr);
    }

    public static function getDistinctElements()
    {
        return collect(SolutionGroup::ELEMENTS)->map(function ($elem) {
            return (new Solution)->getTable() . '.' . $elem;
        })->toArray();
    }
}
