<?php

namespace App;

use App\Support\ModuleService;
use App\Support\TranslatableFormAttribute;
use Illuminate\Support\Facades\Auth;

/**
 * App\Module
 *
 * @property-read mixed $link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Problem[] $problems
 * @mixin \Eloquent
 * @property int $id
 * @property int $course_id
 * @property string $name
 * @property string $description
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Module whereUpdatedAt($value)
 * @property-read \App\Course $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProgrammingLanguage[] $programmingLanguages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Translation[] $translations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 */
class Module extends TranslatableModel
{
    protected $fillable = ['course_id', 'name', 'description'];
    protected $visible = ['id', 'name', 'description', 'course_id', 'problems', 'user_progress'];
    protected $appends = ['user_progress', 'problems_count', 'solved_problems_count'];

    public function getTranslatableAttributes(): array
    {
        return [
            'name'        => new TranslatableFormAttribute('Name'),
            'description' => new TranslatableFormAttribute('Description', TranslatableFormAttribute::INPUT_TYPE_TEXTAREA),
        ];
    }

    public function getLinkAttribute()
    {
        return action('ModuleController@show', [
            'module_id' => $this->id,
            'course_id' => \Request::route()->parameters()['course_id']
        ]);
    }

    public function problems()
    {
        return $this->belongsToMany(Problem::class)
            ->withPivot('display_id');
    }

    public function solutions()
    {
        return $this->hasMany(Solution::class);
    }

    public function programmingLanguages()
    {
        return $this->belongsToMany(ProgrammingLanguage::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps()->withPivot('completed');
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function getUserProgressAttribute()
    {
        if (\Auth::check()) {
            return app(ModuleService::class)->getProgress(Auth::user(), $this);
        }
        return 0;
    }

    public function getProblemsCountAttribute()
    {
        return app(ModuleService::class)->getProblemCount($this);
    }

    public function getSolvedProblemsCountAttribute()
    {
        if (\Auth::check()) {
            return app(ModuleService::class)->getSolvedProblemCount($this, Auth::user());
        }
        return 0;
    }
}
