<?php

namespace App\Notifications;

use App\ValidationToken;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyToken extends Notification
{
    use Queueable;

    const TYPE_MAP = [
        ValidationToken::TYPE_PHONE => 'sms',
        ValidationToken::TYPE_EMAIL => 'mail'
    ];

    private $token;
    private $successUrl;

    /**
     * Create a new notification instance.
     *
     * @param ValidationToken $token
     * @param string $successUrl
     */
    public function __construct(ValidationToken $token, string $successUrl = '')
    {
        $this->token = $token;
        $this->successUrl = $successUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [self::TYPE_MAP[$this->token->type]];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Для того что-бы подтвердить вашу почту нажмите')
            ->action('тут', $this->successUrl . '/' . $this->token->token)
            ->markdown('emails.email');
    }

    public function toSms($notifiable)
    {
        return "Vash kod: {$this->token->token}";
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
