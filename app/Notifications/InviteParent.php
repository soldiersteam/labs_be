<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InviteParent extends Notification
{
    use Queueable;

    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Приглашение в систему ///')// todo  название
            ->line("Дорог(ой/ая) {$this->data['parent_name']}")
            ->line("{$this->data['name']} приглашает вас потратить денег перейдите по ")
            ->action('ссылке', $this->data['success_url'] . '?' . http_build_query([
                    'name' => rawurlencode($this->data['parent_name']),
                    'email' => rawurlencode($this->data['email'])
                ]))
            ->line('чтобы зарегестрироваться')
            ->markdown('emails.email');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
