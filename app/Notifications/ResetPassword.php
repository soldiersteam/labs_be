<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 5/26/17
 * Time: 11:44 PM
 */

namespace App\Notifications;


use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Input;

class ResetPassword extends \Illuminate\Auth\Notifications\ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Вы получаете это письмо потому что был сделан запрос на сброс пароля')
            ->action('Сбросить пароль', Input::get('success_url') . '/' . $this->token)
            ->line('Если вы не запрашивали сброса пароля дальнейших действий не требуется.')
            ->markdown('emails.email');
    }
}