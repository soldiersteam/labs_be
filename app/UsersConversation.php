<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersConversation
 *
 * @property-read \App\Conversation $conversations
 * @property-read \App\User $users
 * @mixin \Eloquent
 */
class UsersConversation extends Model
{
    public function conversations()
    {
        return $this->belongsTo(Conversation::class,'id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'id');
    }
    

}
