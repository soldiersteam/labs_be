<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Status
 *
 * @property int $id
 * @property string $code
 * @method static \Illuminate\Database\Query\Builder|\App\Status whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Status whereId($value)
 * @mixin \Eloquent
 */
class Status extends Model
{
    protected $fillable = [
        'code',
    ];

    public $timestamps = false;
}
