<?php

namespace App\Providers;

use App\Client;
use App\TestingServer;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app['auth']->viaRequest('userLogin', function ($request) {
            $this->validate($request);
            return $this->authModel(User::class, $request);
        });

        $this->app['auth']->viaRequest('userAuth', function (Request $request) {
            return User::getByToken($request->bearerToken());
        });

        $this->app['auth']->viaRequest('clientLogin', function (Request $request) {
            $this->validate($request);
            return $this->authModel(Client::class, $request);
        });

        $this->app['auth']->viaRequest('clientAuth', function (Request $request) {
            return User::getByToken($request->bearerToken());
        });

        $this->app['auth']->viaRequest('serverLogin', function (Request $request) {
            $this->validate($request, 'client_id', 'client_secret');
            return $this->authModel(TestingServer::class, $request, 'client_id', 'client_secret');
        });

        $this->app['auth']->viaRequest('serverAuth', function (Request $request) {
            return TestingServer::getByToken($request->bearerToken());
        });

        $this->registerPolicies();

        Gate::define('access-role-routes', function ($user, $roles) {
            $roles = is_array($roles) ? $roles : array_slice(func_get_args(), 1);
            return $user->hasRole($roles);
        });
    }

    private function validate(Request $request, $login_param = 'login', $pass_param = 'password')
    {
        $validator = Validator::make($request->all(), [
            $login_param => 'required',
            $pass_param => 'required'
        ]);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    private function authModel($class, $request, $login_param = 'login', $pass_param = 'password')
    {
        $model = forward_static_call([$class, 'getByLogin'], $request->get($login_param));
        if (!$model || !Hash::check($request->get($pass_param), $model->password)) {
            return null;
        }
        return $model;
    }
}
