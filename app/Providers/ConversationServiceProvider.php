<?php

namespace App\Providers;

use App\Contracts\ConversationRestrictionStateContract;
use App\Contracts\ConversationServiceContract;
use App\Exceptions\UnauthenticatedException;
use App\Support\Conversations\ConversationHelperService;
use App\Support\Conversations\ConversationService;
use App\Support\Conversations\RestrictedConversationService;
use App\Support\Conversations\RestrictionStates\AdminRestrictionState;
use App\Support\Conversations\RestrictionStates\CuratorRestrictionState;
use App\Support\Conversations\RestrictionStates\ParentRestrictionState;
use App\Support\Conversations\RestrictionStates\UserRestrictionState;
use App\User;
use Illuminate\Support\ServiceProvider;

class ConversationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ConversationRestrictionStateContract::class, function ($app) {
            $user = $app['auth']->guard('user_auth')->user();
            if (!$user) {
                throw new UnauthenticatedException();
            }
            $conversation = $app->make(ConversationHelperService::class);
            switch ($user->role) {
                case User::ROLE_CURATOR:
                    return new CuratorRestrictionState($user, $conversation);
                case User::ROLE_USER:
                    return new UserRestrictionState($user, $conversation);
                case User::ROLE_PARENT:
                    return new ParentRestrictionState($user, $conversation);
                case User::ROLE_ADMIN:
                    return new AdminRestrictionState($user, $conversation);
            }
        });
        $this->app->singleton(ConversationServiceContract::class, RestrictedConversationService::class);
    }
}
