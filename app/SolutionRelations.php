<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 10/17/17
 * Time: 2:53 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Concerns\HasRelationships;

trait SolutionRelations
{
    use HasRelationships;

    public function reports()
    {
        return $this->hasMany(SolutionReport::class);
    }

    public function conversations()
    {
        return $this->morphToMany(Conversation::class, 'topic');
    }

    public function problem()
    {
        return $this->belongsTo(Problem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function programming_language()
    {
        return $this->belongsTo(ProgrammingLanguage::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function statuses()
    {
        return $this->belongsToMany(Status::class);
    }

    public function comment()
    {
        return $this->hasOne(Comment::class);
    }
}