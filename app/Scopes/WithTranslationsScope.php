<?php
namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Created by PhpStorm.
 * User: Valentin
 * Date: 20.07.17
 * Time: 01:19
 */
class WithTranslationsScope implements Scope {

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Builder $builder
     */
    public function apply(Builder $builder, Model $model)
    {
        return $builder->with(['translations' => function ($query) {
            return $query->whereHas('languages', function ($s_query) {
                return $s_query->where('code', app()->getLocale());
            });
        }]);
    }
}