<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 5/29/17
 * Time: 7:58 PM
 */

namespace App\Exceptions;


use Illuminate\Auth\AuthenticationException;

class UnauthenticatedException extends AuthenticationException
{

}