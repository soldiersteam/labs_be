<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 5/25/17
 * Time: 8:52 AM
 */

namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\HttpException;

class EmailNotValidatedException extends HttpException
{
    public function __construct($statusCode = 403, $message = null, \Exception $previous = null, array $headers = array(), $code = 0)
    {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }
}