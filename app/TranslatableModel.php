<?php
/**
 * Created by PhpStorm.
 * User: Valentine
 * Date: 20.07.17
 * Time: 01:30
 */

namespace App;


use App\Scopes\WithTranslationsScope;
use Illuminate\Database\Eloquent\Model;

abstract class TranslatableModel extends Model
{
    protected $with = ['translations'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new WithTranslationsScope());
    }

    public function __construct(array $attributes = [])
    {
        $this->makeVisible('translations');
        parent::__construct($attributes);
    }

    public function translations()
    {
        return $this->morphMany(Translation::class, 'translatable');
    }

    public abstract function getTranslatableAttributes(): array;
}