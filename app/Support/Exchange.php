<?php

/**
 * Created by PhpStorm.
 * User: Valentine
 * Date: 22.01.17
 * Time: 17:08
 */

namespace App\Support;


use Illuminate\Support\Facades\Cache;

class Exchange {
    const USD = 2;
    const EUR = 0;

    public static function rate($currency = self::USD) {
        $exchange = Cache::get('cache_exchange');
        /*$exchange = [
            'data' => [
                self::USD => (object)['sale' => 26]
            ],
            'expire_at' => time() + 24 * 60 * 60,
        ];*/
        if(!$exchange || $exchange['expire_at'] < time()) {
            $exchange['data'] = json_decode(file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5'));
            $exchange['expire_at'] = time() + 24 * 60 * 60;

            Cache::put('cache_exchange', $exchange);
        }

        return $exchange['data'][$currency]->sale;
    }

}