<?php

namespace App\Support;

use App\Course;
use App\Module;
use App\User;

/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 3/28/2017
 * Time: 12:16 AM
 */
class ModuleService
{
    public function createOrUpdate(array $data, Module $module, Course $course)
    {
        $data['course_id'] = $course->id;
        app(LanguageService::class)->fillModel($data, $module);
        $module->save();
        return $module;
    }

    public function show($id, $withLatestSolutions, User $user)
    {
        /** @var Module $module */
        $module = Module::with(['problems' => function ($q) use ($id, $withLatestSolutions) {
            if ($withLatestSolutions) {
                $q->withLatestSolutions(\Auth::id());
            }
            $q->withProgrammingLanguages($id);
        }])->findOrFail($id);
        foreach ($module->problems as $problem) {
            $problem->setAttribute('solved', app(ProblemService::class)->isSolved($module, $problem, $user));
        }
        $module->problems->makeVisible(['display_id', 'solved']);
        return $module;
    }

    public function getProgress(User $user, Module $module)
    {
        $problems = $module->problems()
            ->withBestSolutions($user->id)
            ->get();
        $total = 0;
        foreach ($problems as $problem) {
            if (isset($problem->solutions[0])) {
                $total += $problem->solutions[0]->success_percentage;
            }
        }
        return count($problems) ? $total / count($problems) : 0;
    }

    public function getProblemCount(Module $module)
    {
        return $module->problems()->count();
    }

    public function getSolvedProblemCount(Module $module, User $user)
    {
        return $module->problems()
            ->whereHas('solutions', function ($query) use ($user) {
                $query->where('user_id', $user->id)
                    ->solved();
            })
            ->count();
    }
}
