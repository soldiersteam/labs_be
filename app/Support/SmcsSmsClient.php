<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11/16/17
 * Time: 12:41 AM
 */

namespace App\Support;

use GuzzleHttp\Client;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SmcsSmsClient
{
    public function send($notifiable, Notification $notification)
    {
        $client = new Client();

        Log::critical($notification->toSms($notifiable));
        $result = $client->get('https://smsc.ru/sys/send.php', ['query' => [
            'login' => 'valik35',
            'psw' => '123123',
            'phones' => trim($notifiable->phone_number, '+'),
            'mes' => $notification->toSms($notifiable)
        ]
        ])->getBody();
        Log::critical(json_encode($result->getContents()));
        return $result;
    }
}
