<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/5/17
 * Time: 9:29 PM
 */

namespace App\Support;


use Illuminate\Database\Eloquent\Model;
use mikemccabe\JsonPatch\JsonPatch;

class PatchService
{
    public function patch(Model $model, $operations = [])
    {
        $model->fill(JsonPatch::patch($model->attributesToArray(), $operations));
        $model->save();
    }
}