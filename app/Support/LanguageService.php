<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 10/7/17
 * Time: 7:24 PM
 */

namespace App\Support;

use App\Language;
use App\TranslatableModel;

class LanguageService
{
    public function getEditFormList(TranslatableModel $model)
    {
        return Language::all()
            ->prepend((new Language(['name' => 'Original']))->setAttribute('id', Language::ID_ORIGINAL))
            ->map(function (Language $language) use ($model) {
                return new FormLanguage($language->id, $language->name, $model->getTranslatableAttributes());
            });
    }

    public function getAttribute(TranslatableModel $model, string $attribute, int $languageId): string
    {
        if ($languageId === Language::ID_ORIGINAL) {
            return $model->$attribute ?: '';
        }
        return $model->translations()
                ->where('language_id', $languageId)
                ->where('key', $attribute)
                ->first()->value ?? '';
    }

    public function buildFormKey(string $attribute, int $languageId): string
    {
        return $attribute . '[' . $languageId . ']';
    }

    public function fillModel(array $data, TranslatableModel $model)
    {
        $translatableKeys = array_keys($model->getTranslatableAttributes());
        $translations = [];
        foreach ($translatableKeys as $translatableAttribute) {
            foreach ($data[$translatableAttribute] as $languageId => $datum) {
                if ($languageId === Language::ID_ORIGINAL) {
                    $model->$translatableAttribute = $datum;
                } else {
                    $translations[] = ['keys' => ['language_id' => $languageId, 'key' => $translatableAttribute], 'values' => ['value' => $datum]];
                }
            }
            unset($data[$translatableAttribute]);
        }
        $model->fill($data)->save();
        foreach ($translations as $translation) {
            $model->translations()->updateOrCreate($translation['keys'], $translation['values']);
        }
        return $model;
    }
}
