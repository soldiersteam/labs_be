<?php

namespace App\Support;

use App\Exceptions\VerificationException;
use App\Notifications\VerifyToken;
use App\User;
use App\ValidationToken;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11/15/17
 * Time: 11:26 PM
 */
class VerificationService
{

    public function __construct()
    {
    }

    public function sendEmail(User $user, string $success_url)
    {
        $this->verifySend($user, ValidationToken::TYPE_EMAIL, config('verification.email_resend_timeout'));

        $token = ValidationToken::create([
            'token' => $this->getEmailToken(),
            'type' => ValidationToken::TYPE_EMAIL,
            'user_id' => $user->id
        ]);
        $user->notify(new VerifyToken($token, trim($success_url, '/')));
    }

    public function sendSms(User $user)
    {
        $this->verifySend($user, ValidationToken::TYPE_PHONE, config('verification.phone_resend_timeout'));

        $token = ValidationToken::create([
            'token' => $this->getPhoneToken(),
            'type' => ValidationToken::TYPE_PHONE,
            'user_id' => $user->id
        ]);
        $user->notify(new VerifyToken($token));
    }

    public function getLatestToken(User $user, string $type): ?ValidationToken
    {
        return $user->validationTokens()->latest()->where('type', $type)->first();
    }

    public function isUsed(User $user, string $type): bool
    {
        return $user->validationTokens()->where('type', $type)->where('used', true)->exists();
    }

    protected function verifySend(User $user, string $type, int $resendTimeout)
    {
        if ($this->isUsed($user, $type)) {
            throw new VerificationException('Already verified');
        }

        $latestToken = $this->getLatestToken($user, $type);
        if ($latestToken && $latestToken->created_at->addMinutes($resendTimeout)->lt(Carbon::now())) {
            throw new VerificationException('Can\'t resend token yet');
        }
    }

    protected function getPhoneToken(): int
    {
        $min = 10 ** (config('verification.phone_token_length') - 1);
        return random_int($min, $min * 10 - 1);
    }

    protected function getEmailToken()
    {
        return str_random(255);
    }
}
