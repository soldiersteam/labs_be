<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 10/8/17
 * Time: 11:44 PM
 */

namespace App\Support;


class TranslatableFormAttribute
{
    public $name;
    private $inputType;

    const INPUT_TYPE_TEXT = 'text';
    const INPUT_TYPE_TEXTAREA = 'textarea';

    public function __construct($name, $inputType = self::INPUT_TYPE_TEXT)
    {
        $this->name = $name;
        $this->inputType = $inputType;
    }

    public function isText()
    {
        return $this->inputType === self::INPUT_TYPE_TEXT;
    }

    public function isTextArea()
    {
        return $this->inputType === self::INPUT_TYPE_TEXTAREA;
    }
}