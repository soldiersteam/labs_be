<?php

namespace App\Support;

use App\Course;
use App\Module;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 3/28/2017
 * Time: 12:16 AM
 */
class CourseService
{

    protected $childService;

    function __construct(ChildService $childService)
    {
        $this->childService = $childService;
    }

    /**
     * @param $module_id
     * @return bool
     */
    public function checkByModuleId($module_id)
    {
        return $this->checkAccess(Module::findOrFail($module_id)->course, Auth::user());
    }

    /**
     * @param $id
     * @return bool
     */
    public function checkById($id)
    {
        /** @var Course $course */
        $course = Course::findOrFail($id);
        return $this->checkAccess($course, Auth::user());
    }

    /**
     * @param Course $course
     * @param User $user
     * @return bool
     */
    public function checkAccess(Course $course, User $user)
    {
        if ($user->hasRole(User::ROLE_CURATOR)) {
            return true;
        }
        // @todo
        $student_course = $this->childService->getActiveCourse($user->id);
        return $student_course && $course->id === $student_course->id;
    }

    /**
     * @param Course $course
     * @return bool
     */
    public function authUserHasAccess(Course $course)
    {
        if (Auth::guest()) {
            return false;
        }
        return $this->checkAccess($course, Auth::user());
    }

    public function getCoursesWithoutUserAccess($user_id)
    {
        return Course::whereDoesntHave('users', function ($query) use ($user_id) {
            $query->where('course_user.user_id', $user_id);
        });
    }

    public function getAllowedForPurchaseQuery($student_id, $parent_id)
    {
        $student = User::whereKey($student_id)
            ->whereParentId($parent_id)
            ->firstOrFail();
        if ($this->childService->hasActiveCourse($student_id)) {
            abort(422, 'Course already purchased');
        }
        return $student->courses()
            ->where('completed', false)
            ->whereNull('purchased_until')
            ->orWhere('purchased_until', '<', Carbon::now());
    }

    public function getOwned($user, $perPage = 10)
    {
        return $user->courses()
            ->whereNotNull('purchased_until')
            ->where('purchased_until', '<', Carbon::now())
            ->paginate($perPage);
    }

    public function search($str)
    {
        $query = Course::whereKey($str);
        if ($str) {
            $query->orWhere('name', 'like', "%{$str}%");
        }
        return $query->paginate(100);
    }

    public function getProgress(User $user, Course $course)
    {
        $moduleService = app(ModuleService::class);
        $moduleProgressSum = 0;
        foreach ($course->modules as $module) {
            $moduleProgressSum += $moduleService->getProgress($user, $module);
        }
        return $course->modules->count() ? $moduleProgressSum / $course->modules->count() : 0;
    }

    public function getProblemsPerDuration(Course $course, $duration)
    {
        $problemCount = $course->modules()->withCount('problems')->get()->reduce(function ($carry, $item) {
            return $carry + $item->problems_count;
        }, 0);
        return $problemCount / $course->duration * $duration;
    }

    public function getSolvedProblemsCount(Course $course, User $user, $duration)
    {
        return $course->solutions()
            ->where('user_id', $user->id)
            ->where('created_at', '>', Carbon::now()->subDays($duration))
            ->solved()
            ->count();
    }
}