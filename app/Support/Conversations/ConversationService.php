<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/12/17
 * Time: 3:36 PM
 */

namespace App\Support\Conversations;

use App\Contracts\ConversationServiceContract;
use App\Conversation;
use App\Events\ConversationRead;
use App\Events\MessageSent;
use App\Message;
use App\Problem;
use App\Solution;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class ConversationService implements ConversationServiceContract
{
    protected $conversationHelper;

    public function __construct(ConversationHelperService $conversationHelper)
    {
        $this->conversationHelper = $conversationHelper;
    }

    public function send($fromUserId, $toConversationId, $message, Model $topic = null)
    {
        $conversation = Conversation::findOrFail($toConversationId);
        if (!$this->conversationHelper->userHasConversation($fromUserId, $toConversationId)) {
            $conversation->users()->attach($fromUserId);
        }
        $message = new Message(['message' => $message]);
        $message->user()->associate($fromUserId);
        $message->conversation()->associate($conversation);
        if ($topic) {
            $message->topic()->associate($topic);
        }
        $message->save();
        $conversation->touch();
        broadcast(new MessageSent($message));
        return $message;
    }

    public function createConversation($fromUserId, $toUserId = null)
    {
        $conversation = Conversation::create();
        $users = [$fromUserId];
        if ($toUserId) {
            $users[] = $toUserId;
        }
        $conversation->users()->sync($users);
        return $conversation;
    }

    public function getForUser($id, $userId, $withAttr = true)
    {
        $conversation = Conversation::whereKey($id)
            ->where(function ($query) use ($userId) {
                $query->whereHas('users', function ($query) use ($userId) {
                    $query->where('users.id', $userId);
                })->orHas('users', 1);
            })->firstOrFail();
        return $withAttr ? $this->conversationHelper->setAttributes($conversation, $userId) : $conversation;
    }

    public function getPendingConversations($userId, $courseId = null, $perPage = 10)
    {
        $query = Conversation::has('users', 1);
        $this->conversationHelper->applyTopicFilter($query, $courseId);
        return $this->conversationHelper->getCollectionFromQuery($query, $userId, $perPage);
    }

    public function getUserConversations($userId, $courseId = null, $perPage = 10)
    {
        $query = User::find($userId)->conversations();
        $this->conversationHelper->applyTopicFilter($query, $courseId);
        return $this->conversationHelper->getCollectionFromQuery($query, $userId, $perPage);
    }

    public function markConversationAsReadForUser($conversationId, $userId)
    {
        /** @var Conversation $conversation */
        $conversation = Conversation::findOrFail($conversationId);
        if ($conversation->messages()
            ->where('user_id', '<>', $userId)
            ->update(['is_read' => true])) {
            broadcast(new ConversationRead($conversation, $userId));
        }
    }

    public function getMessages($id, $userId, $beforeId = null, $perPage = 20)
    {
        $query = $this->getForUser($id, $userId, false)
            ->messages()
            ->orderBy('created_at', 'desc')
            ->with('user')
            ->take($perPage);
        if ($beforeId) {
            $beforeMessage = Message::findOrFail($beforeId);
            $query->where('created_at', '<', $beforeMessage->created_at)
                ->where('id', '<', $beforeMessage->id);
        }
        $result = $query->get();
        return ['data' => $result, 'ended' => $query->count() === $result->count()];
    }

    public function discuss($fromId, $message, Model $topic = null)
    {
        /** @var User $user */
        $user = User::findOrFail($fromId);
        if ($user->hasRole(User::ROLE_USER)) {
            $conversation = $this->conversationHelper->getOrCreateForStudent($user);
        } else {
            if ($topic instanceof Solution) {
                $conversation = $this->conversationHelper->getOrCreateForStudent($topic->user);
            } else {
                $conversation = new Conversation();
            }
        }

        $this->send($fromId, $conversation->id, $message, $topic);
        return $conversation;
    }

    public function writeToUser($fromId, $message, $toId)
    {
        $conversation = $this->conversationHelper
            ->getDialogBetweenQuery($fromId, $toId)
            ->first();

        if (!$conversation) {
            $conversation = Conversation::create();
            $conversation->users()->sync([$fromId, $toId]);
        }

        $this->send($fromId, $conversation->id, $message);
        return $conversation;
    }
}