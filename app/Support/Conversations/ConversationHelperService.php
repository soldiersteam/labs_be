<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/19/17
 * Time: 2:03 PM
 */

namespace App\Support\Conversations;


use App\Conversation;
use App\Problem;
use App\Solution;
use App\User;
use Illuminate\Database\Eloquent\Relations\Relation;

class ConversationHelperService
{
    public function userHasConversation($userId, $conversationId)
    {
        return !!User::whereKey($userId)
            ->whereHas('conversations', function ($query) use ($conversationId) {
                $query->where('id', $conversationId);
            })
            ->count();
    }

    public function getDialogBetweenQuery($firstId, $secondId)
    {
        return User::find($firstId)
            ->conversations()
            ->whereHas('users', function ($query) use ($secondId) {
                $query->where('id', $secondId);
            })
            ->has('users', 2)
            ->whereNull('topic_id');
    }

    public function getCollectionFromQuery($conversations, $userId, $perPage)
    {
        $conversations = $conversations->with(['users', 'messages' => function ($query) {
            $query->orderBy('created_at', 'desc')
                ->groupBy(\DB::raw('conversation_id DESC'));
        }])
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
        foreach ($conversations as $conversation) {
            $this->setAttributes($conversation, $userId);
        }
        return $conversations;
    }

    public function getConversationUnread($conversation, $userId)
    {
        return $conversation->messages()->where('is_read', false)
            ->where('user_id', '<>', $userId)
            ->count();
    }

    public function setAttributes(Conversation $conversation, $userId)
    {
        if ($this->isDialog($conversation)) {
            $conversation->setAttribute('name', $conversation->users->filter(function ($user) use ($userId) {
                return $user->id !== $userId;
            })->first()->name);
        }

        $conversation->setAttribute('unread_count', $this->getConversationUnread($conversation, $userId));
        $conversation->makeVisible('unread_count');
        return $conversation;
    }

    public function isDialog(Conversation $conversation)
    {
        // todo better way to determine dialog?
        return $conversation->users()->count() === 2 && $conversation->topic_id === null;
    }

    public function isPending(Conversation $conversation)
    {
        return $conversation->users()->count() === 1;
    }

    public function applyTopicFilter($query, $courseId)
    {
        if ($courseId) {
            $query->where(
                function ($orQuery) use ($courseId) {
                    $orQuery->where('topic_type', array_flip(Relation::morphMap())[Problem::class])
                        ->whereHas('problem.modules.course', function ($courseQuery) use ($courseId) {
                            return $courseQuery->where('id', $courseId);
                        })->orWhere('topic_type', array_flip(Relation::morphMap())[Solution::class])
                        ->whereHas('solution.module.course', function ($courseQuery) use ($courseId) {
                            return $courseQuery->where('id', $courseId);
                        });
                }
            );
        }
    }

    public function getOrCreateForStudent(User $user)
    {
        assert($user->role === User::ROLE_USER, 'getting student convo for non student');
        // todo get current curator conversation for user
        return $user->conversations()->first() ?: $user->conversations()->create();
    }
}