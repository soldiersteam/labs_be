<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 7:49 PM
 */

namespace App\Support\Conversations\RestrictionStates;

use App\Exceptions\ConversationException;
use App\User;

class ParentRestrictionState extends BaseRestrictionState
{

    public function verifyDiscuss($fromUserId, $toUserId, $topic = null)
    {
        // TODO: only write to childrens curators
        if ($topic !== null) {
            throw new ConversationException('Parents can\'t discuss topics');
        }
        if ($toUserId && !User::findOrFail($toUserId)->hasRole(User::ROLE_CURATOR)) {
            throw new ConversationException('Parents can\'t start conversation with non-curators');
        }
    }
}
