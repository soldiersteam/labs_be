<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 9:00 PM
 */

namespace App\Support\Conversations\RestrictionStates;


use App\Conversation;
use Doctrine\DBAL\Types\ConversionException;

class CuratorRestrictionState extends BaseRestrictionState
{
    public function verifyDiscuss($fromUserId, $toUserId, $topic = null)
    {
    }

    public function verifySend($fromUserId, $toConversationId, $message)
    {
        $this->verifyCurrentUser($fromUserId);
        if (
            !$this->conversationService->userHasConversation($fromUserId, $toConversationId)
            && !$this->conversationService->isPending(Conversation::findOrFail($toConversationId))
        ) {
            throw new ConversionException('Not part of conversation and conversation not pending');
        }
    }


    public function verifyGetPendingConversations($userId, $courseId)
    {
    }

    public function verifyWriteToUser($fromId, $toId, $message)
    {
        $this->verifyCurrentUser($fromId);
        $this->verifyDiscuss($fromId, $toId, null);
    }

    public function verifyGetForUser($id, $userId)
    {
        $this->verifyCurrentUser($userId);
    }

    public function verifyMarkConversationAsReadForUser($conversationId, $userId)
    {
        $this->verifyCurrentUser($userId);
        if (!$this->conversationService->isPending(Conversation::findOrFail($conversationId))) {
            $this->verifyInConversation($userId, $conversationId);
        }
    }
}
