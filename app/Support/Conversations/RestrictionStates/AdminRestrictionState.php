<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 9:01 PM
 */

namespace App\Support\Conversations\RestrictionStates;


class AdminRestrictionState extends BaseRestrictionState
{
    public function verifySend($fromUserId, $toConversationId, $message)
    {
        $this->verifyInConversation($fromUserId, $toConversationId);
    }

    public function verifyDiscuss($fromUserId, $toUserId, $topic = null)
    {
        // TODO: Implement verifyCreateConversation() method.
    }

    public function verifyMarkConversationAsReadForUser($conversationId, $userId)
    {
        $this->verifyInConversation($userId, $conversationId);
    }

    public function verifyGetForUser($id, $userId)
    {
        // TODO: Implement verifyGetForUser() method.
    }

    public function verifyGetMessages($id, $userId, $beforeId = null)
    {
        // TODO: Implement verifyGetMessages() method.
    }

    public function verifyGetUserConversations($userId, $courseId)
    {
    }

    public function verifyGetPendingConversations($userId, $courseId)
    {
    }
}