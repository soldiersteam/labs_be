<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 7:49 PM
 */

namespace App\Support\Conversations\RestrictionStates;

use App\Exceptions\ConversationException;
use App\Problem;
use App\Solution;
use App\Support\ProblemService;
use App\User;

class UserRestrictionState extends BaseRestrictionState
{

    public function verifyDiscuss($fromUserId, $toUserId, $topic = null)
    {
        if ($topic instanceof Solution && $topic->user_id != $fromUserId) {
            throw new ConversationException('Student can\'t discuss other\'s solutions');
        } elseif ($topic instanceof Problem && !\App::make(ProblemService::class)->userCanSolve($fromUserId, $topic->id)) {
            throw new ConversationException('Student can\'t discuss this problem');
        }
        if ($toUserId && !$this->conversationService->getDialogBetweenQuery($fromUserId, $toUserId)->count()) {
            throw new ConversationException('Student can\'t start conversations with curators');
        }
//        if ($toUserId && !User::findOrFail($toUserId)->hasRole(User::ROLE_CURATOR)) {
//            throw new ConversationException('Student can\'t start conversation with non-curators');
//        }
    }
}