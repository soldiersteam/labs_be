<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 7:50 PM
 */

namespace App\Support\Conversations\RestrictionStates;


use App\Contracts\ConversationRestrictionStateContract;
use App\Conversation;
use App\Exceptions\ConversationException;
use App\Support\Conversations\ConversationHelperService;
use App\User;
use Doctrine\DBAL\Types\ConversionException;

abstract class BaseRestrictionState implements ConversationRestrictionStateContract
{
    protected $user, $conversationService;

    public function __construct(User $user, ConversationHelperService $conversationService)
    {
        $this->user = $user;
        $this->conversationService = $conversationService;
    }

    public function verifySend($fromUserId, $toConversationId, $message)
    {
        $this->verifyCurrentUser($fromUserId);
        $this->verifyInConversation($fromUserId, $toConversationId);
    }

    public function verifyMarkConversationAsReadForUser($conversationId, $userId)
    {
        $this->verifyCurrentUser($userId);
        $this->verifyInConversation($userId, $conversationId);
    }

    public function verifyWriteToUser($fromId, $toId, $message)
    {
        throw new ConversationException('User can\'t start conversations');
    }

    protected function verifyCurrentUser($userId)
    {
        if ($this->user->id !== $userId) {
            throw new ConversationException('User id not current user');
        }
    }

    protected function verifyInConversation($userId, $conversationId)
    {
        if (!$this->conversationService->userHasConversation($userId, $conversationId)) {
            throw new ConversationException('User not part of conversation');
        }
    }

    public function verifyGetForUser($id, $userId)
    {
        $this->verifyCurrentUser($userId);
        if ($this->conversationService->isPending(Conversation::findOrFail($id))) {
            throw new ConversionException('User can\'t view pending conversation');
        }
    }

    public function verifyGetMessages($id, $userId, $beforeId = null)
    {
        $this->verifyCurrentUser($userId);
    }

    public function verifyGetUserConversations($userId, $courseId)
    {
        $this->verifyCurrentUser($userId);
    }

    public function verifyGetPendingConversations($userId, $courseId)
    {
        throw new ConversationException('User can\'t get pending conversations');
    }
}
