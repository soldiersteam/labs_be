<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 6:58 PM
 */

namespace App\Support\Conversations;


use App\Contracts\ConversationRestrictionStateContract;
use App\Contracts\ConversationServiceContract;
use App\Exceptions\ConversationException;
use App\Problem;
use App\Solution;
use Illuminate\Database\Eloquent\Model;

class RestrictedConversationService implements ConversationServiceContract
{
    const TOPICS = [
        Solution::class,
        Problem::class,
    ];

    protected $conversationService;
    protected $conversationRestrictionState;

    public function __construct(ConversationService $conversationService, ConversationRestrictionStateContract $conversationRestrictionState)
    {
        $this->conversationService = $conversationService;
        $this->conversationRestrictionState = $conversationRestrictionState;
    }

    public function send($fromUserId, $toConversationId, $message)
    {
        $this->conversationRestrictionState->verifySend($fromUserId, $toConversationId, $message);
        return $this->conversationService->send($fromUserId, $toConversationId, $message);
    }

    public function createConversation($fromUserId, $toUserId = null, $topic = null)
    {
        $this->checkTopic($topic);
        $this->conversationRestrictionState->verifyDiscuss($fromUserId, $toUserId, $topic);
        return $this->conversationService->createConversation($fromUserId, $topic, $toUserId);
    }

    public function getUserConversations($userId, $courseId = null, $perPage = 10)
    {
        $this->conversationRestrictionState->verifyGetUserConversations($userId, $courseId);
        return $this->conversationService->getUserConversations($userId, $courseId, $perPage);
    }

    public function getForUser($id, $userId)
    {
        $this->conversationRestrictionState->verifyGetForUser($id, $userId);
        return $this->conversationService->getForUser($id, $userId);
    }

    public function markConversationAsReadForUser($conversationId, $userId)
    {
        $this->conversationRestrictionState->verifyMarkConversationAsReadForUser($conversationId, $userId);
        $this->conversationService->markConversationAsReadForUser($conversationId, $userId);
    }

    public function getMessages($id, $userId, $beforeId = null, $perPage = 20)
    {
        $this->conversationRestrictionState->verifyGetMessages($id, $userId, $beforeId);
        return $this->conversationService->getMessages($id, $userId, $beforeId, $perPage);
    }

    public function discuss($fromId, $message, Model $topic = null)
    {
        $this->conversationRestrictionState->verifyDiscuss($fromId, null, $topic);
        return $this->conversationService->discuss($fromId, $message, $topic);
    }

    public function writeToUser($fromId, $message, $toId)
    {
        $this->conversationRestrictionState->verifyWriteToUser($fromId, $toId, $message);
        return $this->conversationService->writeToUser($fromId, $toId, $message);
    }

    protected function checkTopic($topic)
    {
        if (!in_array(get_class($topic), static::TOPICS) && $topic !== null) {
            throw new ConversationException('Only problem and solution can be topics of conversation');
        }
    }

    public function getPendingConversations($userId, $courseId = null, $perPage = 10)
    {
        $this->conversationRestrictionState->verifyGetPendingConversations($userId, $courseId);
        return $this->conversationService->getPendingConversations($userId, $courseId, $perPage);
    }
}
