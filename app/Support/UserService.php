<?php

namespace App\Support;

use App\Notifications\InviteParent;
use App\Notifications\VerifyEmail;
use App\User;
use Illuminate\Http\UploadedFile;
use Notification;

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 5/11/17
 * Time: 9:14 PM
 */
class UserService
{
    private $verificationService;
    public function __construct(VerificationService $verificationService)
    {
        $this->verificationService = $verificationService;
    }

    /**
     * @param array $data
     * @param User $parent
     * @return User
     */
    public function create(array $data, User $parent = null)
    {
        $user = new User(
            $data + [
                'api_token' => '',
            ]);
        if ($parent) {
            $user->parent()->associate($parent);
        }
        $user->save();
        $this->verificationService->sendSms($user);
        return $user;
    }



    /**
     * @param $token
     */
    public function verifyByToken($token)
    {
        $user = User::where('email_token', $token)->firstOrFail();
        $user->email_token = '';
        $user->save();
    }

    public function update(User $user, array $data, $image)
    {
        $data = collect($data);
        if ($data->has('password')) {
            $user->password = $data->get('password');
        }
        if ($image) {
            \Storage::delete($user->getOriginal('avatar'));
            // @todo try catch
            /** @var $image UploadedFile */
            $user->avatar = $image->store('public/userdata/avatars');
        }
        $user->fill($data->only(['name', 'email'])->toArray());
        /*if ($user->isDirty('email')) {
            $user->email_validated = false;
             @todo
            $this->sendVerification($user);
        }*/
        $user->save();
    }

    public function inviteParent(array $data)
    {
        Notification::send(new User(['email' => $data['email']]), new InviteParent($data));
    }
}