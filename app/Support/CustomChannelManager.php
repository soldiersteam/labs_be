<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11/16/17
 * Time: 12:36 AM
 */

namespace App\Support;

use Illuminate\Notifications\ChannelManager;

class CustomChannelManager extends ChannelManager
{
    public function createSmsDriver()
    {
        return new SmcsSmsClient();
    }
}
