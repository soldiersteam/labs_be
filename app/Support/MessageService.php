<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/13/17
 * Time: 12:47 AM
 */

namespace App\Support;

use App\Message;
use App\User;

class MessageService
{
    public function getUnread(User $user)
    {
        return Message::whereHas('conversation.users', function ($query) use ($user) {
            $query->where('users.id', $user->id);
        })
            ->where('messages.user_id', '<>', $user->id)
            ->whereIsRead(false)
            ->get();
    }
}