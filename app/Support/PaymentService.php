<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 5/30/17
 * Time: 11:34 PM
 */

namespace App\Support;


use App\Course;
use App\Support\ChildService;
use App\Support\CourseService;
use App\Support\UserService;
use App\Transaction;
use App\User;
use ReflectionObject;

class PaymentService
{
    protected $courseService;
    protected $childService;

    public function __construct(CourseService $courseService, ChildService $childService)
    {
        $this->courseService = $courseService;
        $this->childService = $childService;
    }

    public function prepareCourseBuyData(User $user, Course $course, $student_id)
    {
        if (!$this->childService->canBuy($student_id, $course->id) || $this->childService->hasActiveCourse($student_id)) {
            abort(422, 'Курс уже куплен');
        }
        $liqpay = new \LiqPay(env('LIQPAY_PUBLIC_KEY'), env('LIQPAY_PRIVATE_KEY'));

        $transaction = new Transaction();
        $transaction->save();

        $params = [
            'action' => 'pay',
            'sandbox' => '1',
            'amount' => $course->price,
            'currency' => 'UAH',
            'description' => $course->name,
            'order_id' => implode('_', [
                $transaction->id,
                'course', $course->id,
                'user', $user->id,
                'student', $student_id,
                'time', time()
            ]),
            'version' => '3',
            'server_url' => action('TransactionController@callback'),
        ];
        $reflector = new ReflectionObject($liqpay);
        $method = $reflector->getMethod('cnb_params');
        $method->setAccessible(true);
        $params = $method->invoke($liqpay, $params);
        return [
            'data' => base64_encode(json_encode($params)),
            'signature' => $liqpay->cnb_signature($params)
        ];
    }
}