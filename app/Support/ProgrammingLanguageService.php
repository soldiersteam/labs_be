<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 8/8/17
 * Time: 12:18 AM
 */

namespace App\Support;


use App\ProgrammingLanguage;

class ProgrammingLanguageService
{
    public function search($str)
    {
        $query = ProgrammingLanguage::whereKey($str);
        if($str) {
            $query->orWhere('title', 'like', "%{$str}%");
        }
        return $query->paginate(100);
    }
}