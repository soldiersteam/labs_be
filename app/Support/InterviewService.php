<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/29/17
 * Time: 10:33 PM
 */

namespace App\Support;


use App\Interview;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InterviewService
{
    public function complete($id)
    {
        $interview = Interview::findOrFail($id);
        $interview->completed = true;
        $interview->save();
        return $interview;
    }
}