<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 10/17/17
 * Time: 8:17 PM
 */

namespace App\Support;


use App\Comment;
use App\User;

class CommentService
{
    public function store(array $data, string $text, User $user)
    {
        $comment = new Comment(['text' => $text]);
        unset($data['text']);
        $comment->user()->associate($user);
        $comment->save();
        $query = app(SolutionService::class)->buildQuery($data);
        $query->update(['comment_id' => $comment->id]);
        return $comment;
    }
}