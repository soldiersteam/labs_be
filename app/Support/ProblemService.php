<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/10/17
 * Time: 11:33 PM
 */

namespace App\Support;

use App\Module;
use App\Problem;
use App\User;
use Illuminate\Http\UploadedFile;

class ProblemService
{

    protected $childService;

    public function __construct(ChildService $childService)
    {
        $this->childService = $childService;
    }

    public function importFromFile(UploadedFile $archive)
    {
        $problem = new Problem([
            'name'       => str_replace('.tar.gz', '', $archive->getClientOriginalName()),
            'difficulty' => 1,
            'archive'    => ''
        ]);
        $problem->save();
        $problem->setArchive($archive);
        $problem->save();
        $pharData = new \PharData($problem->getArchivePath(true));
        $filename = $problem->name . '/files/problem.png';
        $pharData->extractTo(sys_get_temp_dir(), $filename);
        \File::move(sys_get_temp_dir() . '/' . $filename, $problem->getImagePath() . 'image.ru.png');
    }

    public function createOrUpdate(array $data, Problem $problem, UploadedFile $file = null)
    {
        app(LanguageService::class)->fillModel($data, $problem);

        if ($file && $file->isValid()) {
            $problem->setArchive($file);
        }

        $problem->save();
    }

    public function importManyFromFile($archives)
    {
        foreach ($archives as $archive) {
            $this->importFromFile($archive);
        }
    }

    public function userCanSolve($userId, $problemId)
    {
        if (User::findOrFail($userId)->hasRole(User::ROLE_CURATOR)) {
            return true;
        }
        $course = $this->childService->getActiveCourse($userId);
        if (!$course) {
            return false;
        }
        return !!$course->modules()->whereHas('problems', function ($query) use ($problemId) {
            $query->where('problems.id', $problemId);
        })
            ->count();
    }

    public function isSolved(Module $module, Problem $problem, User $user)
    {
        return !!$module->solutions()
            ->where('problem_id', $problem->id)
            ->where('user_id', $user->id)
            ->solved()
            ->count();
    }
}