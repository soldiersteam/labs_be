<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/17/17
 * Time: 3:43 PM
 */

namespace App\Support;

use App\Course;
use App\Solution;
use App\SolutionGroup;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\UnauthorizedException;

class SolutionService
{
    protected $problemService;
    protected static $SORTS;
    const BASIC_SORTS = ['comment_id'];

    public function __construct(ProblemService $problemService)
    {
        static::$SORTS = [
            'problem'              => function ($query, $orderDir) {
                $query->orderBy('problems.name', $orderDir);
            },
            'user'                 => function ($query, $orderDir) {
                $query->orderBy('users.name', $orderDir);
            },
            'programming_language' => function ($query, $orderDir) {
                $query->orderBy('programming_languages.title', $orderDir);
            },
            'course'               => function ($query, $orderDir) {
                $query->orderBy('courses.name', $orderDir);
            },
        ];
        $this->problemService = $problemService;
    }

    public function create(User $user, Request $request)
    {
        if (!$this->problemService->userCanSolve($user->id, $request->problem_id)) {
            throw new UnauthorizedException();
        }
        $solution = new Solution($request->all());
        $solution->user_id = $user->id;
        $solution->save();

        if ($request->hasFile('solution_code_file')) {
            $solution->saveCodeFile('solution_code_file');
        } else {
            File::put($solution->sourceCodeFilePath(), $request->get('solution_code'));
        }

        return $solution;
    }

    public function single($id)
    {
        return Solution::with('user', 'reports', 'programming_language')->findOrFail($id)
            ->makeVisible(['max_memory', 'max_time', 'successful_reports', 'code']);
    }

    public function filter($perPage, $courses, $programmingLanguages, $students, $orderBy, $orderDir, $page = 1)
    {
        $query = SolutionGroup::query()
            ->join('problems', 'solutions.problem_id', '=', 'problems.id')
            ->join('programming_languages', 'solutions.programming_language_id', '=', 'programming_languages.id')
            ->join('users', 'solutions.user_id', '=', 'users.id')
            ->join('modules', 'solutions.module_id', '=', 'modules.id')
            ->leftJoin('comments', 'solutions.comment_id', '=', 'comments.id')
            ->join('courses', 'solutions.course_id', '=', 'courses.id');

        if ($courses) {
            $query->whereIn('solutions.course_id', $courses);
        }

        if ($students) {
            $query->whereIn('solutions.user_id', $students);
        }

        if ($programmingLanguages) {
            $query->whereIn('solutions.programming_language_id', $programmingLanguages);
        }
        $query->groupBy(SolutionGroup::getDistinctElements())
            ->select(SolutionGroup::getSelects());


        $count = DB::table(DB::raw("({$query->toSql()}) as sub"))
            ->mergeBindings($query->getQuery())
            ->count();

        if ($orderBy) {
            if (in_array($orderBy, static::BASIC_SORTS)) {
                $query->orderBy($orderBy, $orderDir);
            } elseif (array_key_exists($orderBy, static::$SORTS)) {
                static::$SORTS[$orderBy]($query, $orderDir);
            }
        } else {
            $query->orderBy(DB::raw('max(solutions.created_at)'), 'desc');
        }


        return new LengthAwarePaginator(
            $query->skip($perPage * ($page - 1))->take($perPage)->get(),
            $count,
            $perPage,
            $page
        );
    }

    public function getSolutionsForGroup(array $group)
    {

        $solutions = $this->buildQuery($group)->with('statuses')
            ->orderBy('solutions.created_at', 'desc')->get();
        foreach ($solutions as $item) {
            $item->setVisible(['id', 'created_at', 'state', 'statuses', 'success_percentage']);
        }
        return $solutions;
    }

    public function getCountForDays(User $user, Course $course, $days)
    {
        $items = $course->solutions()
            ->where('user_id', $user->id)
            ->where('solutions.created_at', '>=', Carbon::now()->subDays($days))
            ->groupBy('name')
            ->get([DB::raw('DATE(solutions.created_at) AS name'), DB::raw('COUNT(solutions.id) AS value')])
            ->makeHidden(['course_id', 'programming_language', 'statuses', 'code']);
        return $this->fillSolutionActivity($days, $items->toArray());
    }

    public function fillSolutionActivity($days, $items = [])
    {
        foreach (new \DatePeriod(Carbon::now()->subDays($days - 1), new \DateInterval('P1D'), Carbon::now()->addDay()) as $date) {
            if (!count(array_filter($items, function ($item) use ($date) {
                return $item['name'] === $date->format('Y-m-d');
            }))) {
                $items[] = ['name' => $date->format('Y-m-d'), 'value' => 0];
            }
        }
        usort($items, function ($a, $b) {
            return $a['name'] <=> $b['name'];
        });
        return $items;
    }

    public function buildQuery(array $group)
    {
        $query = Solution::query();

        foreach (SolutionGroup::ELEMENTS as $element) {
            if ($group[$element]) {
                $query->where($element, $group[$element]);
            } else {
                $query->whereNull($element);
            }
        }
        return $query;
    }
}
