<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11/16/17
 * Time: 12:41 AM
 */

namespace App\Support;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class TurboSmsClient
{
    public function send($notifiable, Notification $notification)
    {
        $client = new \SoapClient('http://turbosms.in.ua/api/wsdl.html');

        Log::critical($notification->toSms($notifiable));
        $client->Auth([ //@todo: to ENV
            'login' => 'qbit_online',
            'password' => 'prolabs'
        ]);

        $result = $client->SendSMS([
            'sender' => 'Msg',
            'destination' => $notifiable->phone_number,
            'text' => $notification->toSms($notifiable)
        ]);

        Log::critical(json_encode($result));
        return $result;
    }
}
