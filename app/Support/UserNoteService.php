<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 9/23/17
 * Time: 9:20 PM
 */

namespace App\Support;


use App\User;
use App\UserNote;

class UserNoteService
{
    public function getAboutUserQuery($userId)
    {
        return User::findOrFail($userId)->notesAbout();
    }

    public function store(User $author, $aboutUserId, $message)
    {
        return User::findOrFail($aboutUserId)->notesAbout()->create(['message' => $message, 'author_id' => $author->id]);
    }

    public function delete(UserNote $note)
    {
        return $note->delete();
    }
}