<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/29/17
 * Time: 10:33 PM
 */

namespace App\Support;


use App\Course;
use App\Exceptions\ChildHasCourseException;
use App\Interview;
use App\Notifications\CoursesSelected;
use App\Notifications\HaveInterview;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ChildService
{
    const PROGRESS_FOR_DAYS = 7;
    const ACADEMIC_PERFORMANCE_DAYS = 7;

    public function getChild($id)
    {
        return User::whereKey($id)
            ->whereRole(User::ROLE_USER)
            ->firstOrFail();
    }

    // какую-то муть тут наваял
    public function schedule($id, $parent_id)
    {
        $child = User::whereKey($id)
            ->whereRole(User::ROLE_USER)
            ->whereParentId($parent_id)
            ->firstOrFail();

        // curator without any interviews (everyone goes once)
        $curator = User::whereRole(User::ROLE_CURATOR)
            ->whereDoesntHave('curatorInterviews')
            ->first();
        if (!$curator) {
            // curator with id > last added interview curator_id (everyone goes again)
            $curator = User::whereRole(User::ROLE_CURATOR)
                ->where('id', '>', function ($query) {
                    $query->selectRaw('max(curator_id)')->from('interviews')
                        ->whereIn('id', function ($maxIdQuery) {
                            $maxIdQuery->selectRaw('max(id)')
                                ->from('interviews');
                        });
                })
                ->first();
        }
        if (!$curator) {
            // select curator with min(id) (restart loop)
            $curator = User::whereKey(function ($query) {
                $query->selectRaw('min(id)')
                    ->whereRole(User::ROLE_CURATOR)
                    ->from('users');
            })
                ->first();
        }
        if (!$curator) {
            // something went wrong
            throw new ModelNotFoundException();
        }
        $interview = new Interview();
        $interview->curator()->associate($curator);
        $interview->student()->associate($child);
        $interview->save();
        $curator->notify(new HaveInterview());
        return $interview;
    }

    public function markAsInterviewed($child_id, $curator_id)
    {
        $interview = $this->getChild($child_id)->studentInterviews()
            ->where('curator_id', $curator_id)
            ->firstOrFail();
        $interview->completed = true;
        $interview->save();
        return $interview;
    }

    public function givePurchaseAccess($child_id, $course_ids, $success_url = null)
    {
        $child = User::whereKey($child_id)
            ->whereRole(User::ROLE_USER)
            ->whereDoesntHave('courses', function ($query) use ($course_ids) {
                $query->whereIn('course_id', $course_ids);
            })->first();
        if (!$child) {
            throw new ChildHasCourseException();
        }
        $child->courses()->attach($course_ids, ['purchased_until' => null]);
        if ($success_url) {
            $child->parent->notify(new CoursesSelected($success_url));
        }
    }

    public function canBuy($student_id, $course_id)
    {
        return !!User::whereKey($student_id)
            ->whereHas('courses', function ($query) use ($course_id) {
                $query->where('course_id', $course_id)
                    ->whereNull('purchased_until')
                    ->orWhere('purchased_until', '<', Carbon::now());
            })
            ->count();
    }

    public function canBuyAny($student_id)
    {
        return !$this->hasActiveCourse($student_id)
            && $this->getChild($student_id)
                ->courses()->count();
    }

    public function hasActiveCourse($student_id)
    {
        return !!User::whereKey($student_id)->whereHas('courses', function ($query) {
            $query->where('purchased_until', '>=', Carbon::now());
        })->count();
    }

    /**
     * @param $student_id
     * @return Course | null
     */
    public function getActiveCourse($student_id)
    {
        return $this->getChild($student_id)
            ->courses()
            ->where('purchased_until', '>', Carbon::now())
            ->first();
    }

    public function attachCourse($student_id, $course_id)
    {
        $course = Course::findOrFail($course_id);
        User::find($student_id)
            ->courses()
            ->updateExistingPivot($course_id, ['purchased_until' => Carbon::now()->addDays($course->duration)]);
    }

    public function search($str)
    {
        $query = User::whereRole(User::ROLE_USER)
            ->where(function ($query) use ($str) {
                $query->whereKey($str);
                if ($str) {
                    $query->orWhere('name', 'like', "%{$str}%");
                }
            });

        return $query->paginate(100);
    }

    public function getProgress(User $user)
    {
        /** @var Course $course */
        $course = $this->getActiveCourse($user->id);

        if ($course) {
            return [
                'courseProgress'      => app(CourseService::class)->getProgress($user, $course),
                'solutionsActivity'   => app(SolutionService::class)->getCountForDays($user, $course, self::PROGRESS_FOR_DAYS),
                'academicPerformance' => $this->getAcademicPerformance($user, $course)
            ];
        }
        return [
            'courseProgress'      => 0,
            'solutionsActivity'   => app(SolutionService::class)->fillSolutionActivity(self::PROGRESS_FOR_DAYS),
            'academicPerformance' => 0
        ];
    }

    public function getAcademicPerformance(User $user, Course $course)
    {
        $courseService = app(CourseService::class);
        $shouldSolve = $courseService->getProblemsPerDuration($course, self::ACADEMIC_PERFORMANCE_DAYS);
        assert($shouldSolve != 0, 'Problems per duration should not be 0');
        $hasSolved = $courseService->getSolvedProblemsCount($course, $user, self::ACADEMIC_PERFORMANCE_DAYS);

        return $hasSolved / $shouldSolve * 100;
    }
}
