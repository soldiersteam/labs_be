<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 10/7/17
 * Time: 7:43 PM
 */

namespace App\Support;


class FormLanguage
{
    public $id;
    public $name;
    public $attributes = [];

    public function __construct($id, $name, array $attributes)
    {
        $this->id = $id;
        $this->name = $name;
        $this->attributes = $attributes;
    }
}