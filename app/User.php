<?php

namespace App;

use App;
use App\Notifications\ResetPassword;
use App\Support\ChildService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property mixed $password
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $role
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @property int $parent_id
 * @property string $api_token
 * @property string $token_created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Conversation[] $conversations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Interview[] $curatorInterviews
 * @property-read mixed $can_buy
 * @property-read mixed $course_id
 * @property-read mixed $is_scheduled
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Module[] $modules
 * @property-read \App\User $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Solution[] $solutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Interview[] $studentInterviews
 * @method static \Illuminate\Database\Query\Builder|\App\User whereApiToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmailToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmailValidated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTokenCreatedAt($value)
 */
class User extends AuthenticatableModel
{
    use Notifiable;

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_CURATOR = 'curator';
    const ROLE_PARENT = 'parent';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'role', 'api_token', 'phone_number'
    ];

    protected $appends = ['is_scheduled', 'can_buy', 'course_id', 'progress'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'progress',
        'api_token', 'token_created_at', 'is_scheduled', 'can_buy', 'course_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    public static function roles()
    {
        return [
            self::ROLE_ADMIN,
            self::ROLE_USER,
            self::ROLE_CURATOR,
            self::ROLE_PARENT
        ];
    }

    public static function getByLogin($login)
    {
        return static::where('email', $login)
            ->orWhere('name', $login)->first();
    }

    public function parent()
    {
        return $this->belongsTo(User::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(User::class, 'parent_id');
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class)->withTimestamps()->withPivot('completed');
    }

    public function solutions()
    {
        return $this->hasMany(Solution::class);
    }

    public function curatorInterviews()
    {
        return $this->hasMany(Interview::class, 'curator_id');
    }

    public function studentInterviews()
    {
        return $this->hasMany(Interview::class, 'student_id');

    }

    public function getIsScheduledAttribute()
    {
        return (bool)$this->studentInterviews()->count();
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function conversations()
    {
        return $this->belongsToMany(Conversation::class);
    }

    public function hasAccessToCourse(Course $course)
    {
        return (bool)$this->courses()->find($course->id);
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class)->withTimestamps();
    }

    public function notesAbout()
    {
        return $this->hasMany(UserNote::class);
    }

    public function notesWrittenBy()
    {
        return $this->hasMany(UserNote::class, 'author_id');
    }

    public function validationTokens()
    {
        return $this->hasMany(ValidationToken::class);
    }

    public function hasRole($roles)
    {
        if (is_array($roles)) {
            return array_search($this->role, $roles) !== false;
        } else {
            return $this->role == $roles;
        }
    }

    public function resetToken()
    {
        $this->api_token = '';
        return $this;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function getCanBuyAttribute()
    {
        return App::make(ChildService::class)->canBuyAny($this->id);
    }

    public function getCourseIdAttribute()
    {
        $course = App::make(ChildService::class)->getActiveCourse($this->id);
        return $course ? $course->id : null;
    }

    public function getAvatarAttribute()
    {
        return $this->attributes['avatar'] ? asset(Storage::url($this->attributes['avatar'])) : '';
    }

    public function getProgressAttribute()
    {
        return app(ChildService::class)->getProgress($this);
    }
}
