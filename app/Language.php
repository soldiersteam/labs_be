<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * App\Language
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $static_text
 * @property-write mixed $static_text_file
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Translation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Language whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Language whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Language whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Language whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Language extends Model
{
    const ID_ORIGINAL = 0;

    protected $fillable = ['code', 'name'];

    protected function getStaticTextDirectory($absolute = true)
    {
        $relative_path = 'languages/' . $this->id . '/';
        $dir = storage_path('app/' . $relative_path);
        if (!\File::exists($dir)) {
            \File::makeDirectory($dir, 0755, true);
        }
        if ($absolute) {
            return $dir;
        }
        return $relative_path;
    }

    public function translations()
    {
        return $this->hasMany(Translation::class);
    }

    public function getStaticTextAttribute()
    {
        $dir = $this->getStaticTextDirectory();
        if (!\File::exists($dir . 'static.json')) {
            \File::put($dir . 'static.json', '');
        }

        $translations = \File::get($dir . 'static.json');
        if (!$translations) {
            return '{}';
        }
        return $translations;
    }

    public function setStaticTextFileAttribute(UploadedFile $file)
    {
        $file->storeAs($this->getStaticTextDirectory(false), 'static.json');
    }

    public function getCodeAttribute()
    {
        return strtolower($this->attributes['code']);
    }

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = strtolower($value);
    }
}
