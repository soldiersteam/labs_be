<?php

namespace App\Http\Requests\Api;

use App\Solution;

class SolutionPatchRequest extends PatchRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge([
            '*.op' => 'in:replace',
            '*.path' => 'in:/state',
            '*.value' => 'in:' . implode(',', Solution::getStates())
        ], parent::rules());
    }
}
