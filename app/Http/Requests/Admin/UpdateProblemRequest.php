<?php

namespace App\Http\Requests\Admin;

use App\Language;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProblemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.' . Language::ID_ORIGINAL => 'required',
            'name.*'                        => 'string|max:255',
            'difficulty'                    => 'required|integer|between:0,5',
            'archive'                       => ['mimetypes:application/x-gzip'],
        ];
    }
}
