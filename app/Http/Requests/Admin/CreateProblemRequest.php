<?php

namespace App\Http\Requests\Admin;

class CreateProblemRequest extends UpdateProblemRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['archive'][] = 'required';
        return $rules;
    }
}
