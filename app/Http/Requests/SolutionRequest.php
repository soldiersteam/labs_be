<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SolutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id' => 'required|exists:modules,id',
            'problem_id' => 'required|exists:problems,id',
            'programming_language_id' => ['required',
                Rule::exists('module_problem_programming_language')->where(function ($query) {
                    $query->where('module_id', $this->get('module_id'))
                        ->where('problem_id', $this->get('problem_id'));
                })],
            'solution_code' => 'required_without:solution_code_file',
            'solution_code_file' => 'required_without:solution_code|mimetypes:text/plain',
        ];
    }
}
