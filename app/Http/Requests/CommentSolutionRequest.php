<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentSolutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'                    => 'required|max:3000',
            'problem_id'              => 'required|exists:problems,id',
            'programming_language_id' => 'required|exists:programming_languages,id',
            'user_id'                 => 'required|exists:users,id',
            'module_id'               => 'required|exists:modules,id',
            'course_id'               => 'required|exists:courses,id',
        ];
    }
}
