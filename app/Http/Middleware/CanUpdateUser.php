<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CanUpdateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws AccessDeniedHttpException
     */
    public function handle($request, Closure $next)
    {
        $user = User::findOrFail($request->route('user_id'));
        $isCurrentCurator = Auth::user()->hasRole(User::ROLE_CURATOR) && \Auth::user()->is($user);
        $parentUpdateChild = Auth::user()->hasRole(User::ROLE_PARENT) && Auth::user()->children()->where('id', $user->id)->count();
        if($isCurrentCurator || $parentUpdateChild) {
            return $next($request);
        }
        throw new AccessDeniedHttpException('User can\'t update');
    }
}
