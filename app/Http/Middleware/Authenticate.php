<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthenticationFailedException;
use App\Exceptions\UnauthenticatedException;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @param  bool $login
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $login = true)
    {
        if (Auth::guard($guard)->guest()) {
            $this->handleUnauthorizedRequest($request, $login);
        } else {
            return $next($request);
        }
    }

    protected function handleUnauthorizedRequest($request, $login = false)
    {
        if ($login) {
            throw new AuthenticationFailedException();
        }
        throw new UnauthenticatedException();
    }
}