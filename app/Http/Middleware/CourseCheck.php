<?php

namespace App\Http\Middleware;

use App\Course;
use App\Module;
use App\Support\CourseService;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CourseCheck
{
    private $courseService;

    function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string $by
     * @return mixed
     */
    public function handle($request, Closure $next, $by = null)
    {
        switch ($by) {
            case 'module_id':
                $has_access = $this->courseService->checkByModuleId($request->module_id);
                break;
            case 'course_id':
                $has_access = $this->courseService->checkById($request->get('course_id'));
                break;
            default:
                $has_access = $this->courseService->checkById($request->id);
        }
        if (!$has_access) {
            abort(422, 'Курс не куплен');
        }
        return $next($request);
    }
}
