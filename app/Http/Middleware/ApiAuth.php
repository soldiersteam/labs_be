<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuth extends Authenticate
{
    public function handle($request, Closure $next, $guard = null, $login = false)
    {
        Auth::setDefaultDriver($guard);
        if (Auth::check()) {
            return $next($request);
        }

         $this->handleUnauthorizedRequest($request, $login);
    }
}
