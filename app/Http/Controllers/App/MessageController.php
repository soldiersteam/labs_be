<?php

namespace App\Http\Controllers\App;

use App\Support\MessageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    protected $messageService;

    function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function unread(Request $request)
    {
        return $this->messageService->getUnread(Auth::user());
    }
}
