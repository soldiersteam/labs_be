<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/25/17
 * Time: 5:45 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Support\ChildService;
use App\User;
use Illuminate\Http\Request;

class UserController
{
    protected $childService;

    public function __construct(ChildService $childService)
    {
        $this->childService = $childService;
    }

    public function show(Request $request, $id)
    {
        return $this->childService->getChild($id);
    }

    public function index(Request $request)
    {
        return User::whereRole(User::ROLE_USER)
            ->paginate(10);
    }

    public function giveAccess(Request $request, $id)
    {
        $this->childService->givePurchaseAccess($id, $request->get('course_ids'), $request->get('success_url'));
    }

    public function search(Request $request)
    {
        return $this->childService->search($request->get('query'));
    }
}
