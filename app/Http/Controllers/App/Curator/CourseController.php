<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/25/17
 * Time: 5:45 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Course;
use App\Support\CourseService;
use Illuminate\Http\Request;

class CourseController
{
    protected $courseService;

    function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function index(Request $request)
    {
        if ($request->get('student_id')) {
            return $this->courseService->getCoursesWithoutUserAccess($request->get('student_id'))
                ->paginate(10);
        }
        return Course::paginate(10);
    }

    public function search(Request $request)
    {
        return $this->courseService->search($request->get('query'));
    }
}
