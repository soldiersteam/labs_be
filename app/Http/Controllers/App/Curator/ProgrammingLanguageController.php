<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/2/17
 * Time: 7:46 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Http\Controllers\Controller;
use App\Support\ProgrammingLanguageService;
use Illuminate\Http\Request;

class ProgrammingLanguageController extends Controller
{
    protected $languageService;

    public function __construct(ProgrammingLanguageService $languageService)
    {
        $this->languageService = $languageService;
    }

    public function search(Request $request)
    {
        return $this->languageService->search($request->get('query'));
    }
}