<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/25/17
 * Time: 5:45 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Course;
use App\Problem;
use Illuminate\Http\Request;

class ProblemController
{
    public function index(Request $request)
    {
        return Problem::paginate(10);
    }

    public function show(Request $request, $id)
    {
        return Problem::findOrFail($id);
    }
}
