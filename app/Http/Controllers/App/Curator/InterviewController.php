<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/25/17
 * Time: 5:45 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Course;
use App\Interview;
use App\Support\InterviewService;
use Auth;
use Illuminate\Http\Request;

class InterviewController
{
    protected $interviewService;

    function __construct(InterviewService $interviewService)
    {
        $this->interviewService = $interviewService;
    }

    public function index(Request $request)
    {
        return Auth::user()->curatorInterviews()->paginate(10);
    }

    public function complete(Request $request, $id)
    {
        return $this->interviewService->complete($id);
    }
}
