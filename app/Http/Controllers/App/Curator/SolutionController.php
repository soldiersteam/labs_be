<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/25/17
 * Time: 5:45 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Support\SolutionService;
use Illuminate\Http\Request;

class SolutionController
{
    protected $solutionService;

    public function __construct(SolutionService $solutionService)
    {
        $this->solutionService = $solutionService;
    }

    public function groupIndex(Request $request)
    {
        return $this->solutionService->filter(
            10,
            $request->get('courses'),
            $request->get('programmingLanguages'),
            $request->get('students'),
            $request->get('prop'),
            $request->get('dir'),
            $request->get('page', 1)

        );
    }

    public function index(Request $request)
    {
        return $this->solutionService->getSolutionsForGroup($request->all());
    }
}
