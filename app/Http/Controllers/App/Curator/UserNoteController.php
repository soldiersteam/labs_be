<?php

namespace App\Http\Controllers\App\Curator;

use App\Http\Requests\Api\StoreUserNoteRequest;
use App\Support\UserNoteService;
use App\UserNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserNoteController extends Controller
{
    private $userNoteService;

    public function __construct(UserNoteService $userNoteService)
    {
        $this->userNoteService = $userNoteService;
    }

    public function index(Request $request, $userId)
    {
        return $this->userNoteService->getAboutUserQuery($userId)
            ->with('author')
            ->paginate($request->get('per_page', 10));
    }

    public function store(StoreUserNoteRequest $request, $userId)
    {
        return response(
            $this->userNoteService->store(Auth::user(), $userId, $request->get('message'))->load('author'),
            201
        );
    }

    public function delete(Request $request, $id)
    {
        $note = UserNote::findOrFail($id);
        if (Auth::user()->id == $note->author_id) {
            $this->userNoteService->delete($note);
        } else {
            throw new AccessDeniedHttpException();
        }
    }
}
