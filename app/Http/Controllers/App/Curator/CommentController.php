<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 6/25/17
 * Time: 5:45 PM
 */

namespace App\Http\Controllers\App\Curator;

use App\Http\Requests\CommentSolutionRequest;
use App\Support\CommentService;
use Auth;

class CommentController
{

    public function comment(CommentSolutionRequest $request)
    {
        return app(CommentService::class)->store($request->except('text'), $request->get('text'), Auth::user());
    }
}
