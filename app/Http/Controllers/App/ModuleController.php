<?php

namespace App\Http\Controllers\App;

use App\Module;
use App\Support\ModuleService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    private $moduleService;

    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
    }

    public function index(Request $request)
    {
        return Module::where('course_id', $request->get('course_id'))
            ->paginate($request->get('per_page', 10));
    }

    public function single(Request $request, $id)
    {
        return $this->moduleService->show($id, $request->get('with_latest_solution', false), Auth::user());
    }
}
