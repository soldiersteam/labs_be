<?php

namespace App\Http\Controllers\App\Parent;

use App\Support\UserService;
use App\User;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class RegisterController extends \App\Http\Controllers\App\Auth\RegisterController
{

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    protected function create(array $data)
    {
        return $this->userService->create($data, User::ROLE_USER, Auth::user());
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
