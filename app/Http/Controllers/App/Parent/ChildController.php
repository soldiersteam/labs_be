<?php

namespace App\Http\Controllers\App\Parent;

use App\Http\Controllers\Controller;
use App\Support\ChildService;
use Auth;
use Illuminate\Http\Request;

class ChildController extends Controller
{

    protected $childService;

    function __construct(ChildService $childService)
    {
        $this->childService = $childService;

    }

    public function index(Request $request)
    {
        $c = Auth::user()->children()->paginate(10);
        $c->makeVisible(['is_scheduled', 'can_buy', 'progress']);
        return $c;
    }

    protected function schedule(Request $request, $id)
    {
        return $this->childService->schedule($id, Auth::user()->id);
    }
}
