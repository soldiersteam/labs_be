<?php

namespace App\Http\Controllers\App\Parent;

use App\Course;
use App\Support\CourseService;
use App\Support\PaymentService;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    protected $paymentService;
    protected $courseService;

    public function __construct(PaymentService $paymentService, CourseService $courseService)
    {
        $this->paymentService = $paymentService;
        $this->courseService = $courseService;
    }

    public function index(Request $request)
    {
        return $this->courseService->getAllowedForPurchaseQuery(
            $request->get('student_id'),
            Auth::user()->id
        )->paginate($request->get('per_page', 10));
    }

    public function single(Request $request, $id)
    {
        return Course::findOrFail($id);
    }

    public function getBuyData(Request $request, $id)
    {
        /** @var User $user */
        $user = \Auth::user();
        /** @var Course $course */
        $course = Course::findOrFail($id);
        return $this->paymentService->prepareCourseBuyData($user, $course, $request->get('student_id'));
    }
}
