<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\ProgrammingLanguage;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProgrammingLanguageController extends Controller
{
    /**
     * Returns all programming languages
     *
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request)
    {
        return ProgrammingLanguage::all();
    }

    public function show(Request $request, $id)
    {
        return ProgrammingLanguage::findOrFail($id);
    }
}
