<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 8/5/17
 * Time: 10:43 PM
 */

namespace App\Http\Controllers\App\Student;


use App\Support\CourseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController
{
    protected $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function getOwned(Request $request)
    {
        return $this->courseService->getOwned(Auth::user());
    }
}