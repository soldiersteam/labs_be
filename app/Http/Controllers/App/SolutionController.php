<?php

namespace App\Http\Controllers\App;

use App\Contracts\ConversationServiceContract;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\SolutionRequest;
use App\Solution;
use App\Support\SolutionService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SolutionController extends Controller
{
    private $solutionService, $conversationService;

    public function __construct(SolutionService $solutionService, ConversationServiceContract $conversationService)
    {
        $this->solutionService = $solutionService;
        $this->conversationService = $conversationService;
    }

    public function single(Request $request, $id)
    {

        $solution = $this->solutionService->single($id);
        if (Auth::user()->id != $solution->user_id && !Auth::user()->hasRole(User::ROLE_CURATOR)) {
            throw new AccessDeniedHttpException('User doesn\'t have access to this solution');
        }
        return $solution;
    }

    public function index(Request $request)
    {
        $solutions = Solution::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc');

        if ($request->get('module_id')) {
            $solutions->where('module_id', $request->get('module_id'));
        }
        if ($request->get('problem_id')) {
            $solutions->where('problem_id', $request->get('problem_id'));
        }
        if ($request->get('with_problems')) {
            $solutions->with('problem');
        }

        return $solutions->paginate($request->get('per_page', 10));
    }

    public function create(SolutionRequest $request)
    {
        return $this->solutionService->create(Auth::user(), $request);
    }

    public function reports(Request $request, $id)
    {
        return Solution::findOrFail($id)->reports;
    }

    public function discuss(MessageRequest $request, $id)
    {
        return $this->conversationService->discuss(Auth::user()->id, $request->get('text'), Solution::findOrFail($id));
    }
}
