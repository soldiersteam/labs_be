<?php

namespace App\Http\Controllers\App\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResendVerificationRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\ValidateEmailRequest;
use App\Support\UserService;
use App\User;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $userService;

    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     *
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest');
        $this->userService = $userService;
    }

    public function validateEmail(ValidateEmailRequest $request)
    {
        $this->userService->verifyByToken($request->get('token'));
    }

    public function resend(ResendVerificationRequest $request)
    {
        $this->userService->sendVerification(User::where('email', $request->get('email'))->firstOrFail(), $request->get('success_url'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  UserRegisterRequest $request
     * @return User
     */
    public function register(UserRegisterRequest $request)
    {
        $user = $this->userService->create($request->all());
        event(new Registered($user));
        return response($user, 201);
    }
}
