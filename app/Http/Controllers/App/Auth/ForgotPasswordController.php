<?php

namespace App\Http\Controllers\App\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResendVerificationRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails {
        sendResetLinkEmail as sendMail;
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        abort(520, 'Письмо не было отправлено');
    }

    public function sendResetLinkEmail(ResendVerificationRequest $request)
    {
        $this->sendMail($request);
    }

    protected function sendResetLinkResponse($response)
    {
        return new JsonResponse(trans($response));
    }

    protected function validateEmail(Request $request)
    {
    }
}
