<?php

namespace App\Http\Controllers\App;

use App\Exceptions\IncorrectOldPasswordException;
use App\Http\Controllers\Auth\AuthenticatableController;
use App\Http\Requests\InviteParentRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Support\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class UserController extends AuthenticatableController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function logout(Request $request)
    {
        Auth::user()->resetToken()->save();
    }

    public function getToken(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole(User::ROLE_USER)) {
            $user->makeVisible('course_id');
        }
        return parent::getToken($request) + compact('user');
    }

    public function read(Request $request, $id)
    {
        return User::findOrFail($id);
    }

    public function update(ProfileUpdateRequest $request, $user_id)
    {
        /** @var User $user */
        $user = User::findOrFail($user_id);
        if($request->hasFile('image') && $request->file('image')->isValid()) {
            $file = $request->file('image');
        } else {
            $file = null;
        }
        $this->userService->update($user, $request->all(), $file);

    }

    public function inviteParent(InviteParentRequest $request)
    {
        $this->userService->inviteParent($request->all());
    }
}
