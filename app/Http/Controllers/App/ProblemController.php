<?php

namespace App\Http\Controllers\App;

use App\Contracts\ConversationServiceContract;
use App\Http\Requests\MessageRequest;
use App\Module;
use App\Problem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProblemController extends Controller
{
    protected $conversationService;

    public function __construct(ConversationServiceContract $conversationService)
    {
        $this->conversationService = $conversationService;
    }

    public function index(Request $request)
    {
        $problems = Module::findOrFail($request->get('module_id'))
            ->problems()
            ->withProgrammingLanguages($request->get('module_id'));

        if ($request->get('with_latest_solution', false)) {
            $problems = $problems->withLatestSolutions();
        }
        $problems = $problems->paginate($request->get('per_page', 20));

        $problems->makeVisible('display_id');
        return $problems;
    }

    public function single(Request $request, $id)
    {
        return Problem::whereKey($id)
            ->withProgrammingLanguages($request->get('module_id'))
            ->firstOrFail()->makeVisible('image');
    }


    public function discuss(MessageRequest $request, $id)
    {
        return $this->conversationService->discuss(Auth::user()->id, $request->get('text'), Problem::findOrFail($id));
    }
}
