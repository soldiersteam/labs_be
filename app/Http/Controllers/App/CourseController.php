<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/2/17
 * Time: 7:46 PM
 */

namespace App\Http\Controllers\App;

use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function show(Request $request, $id)
    {
        $course = Course::with('modules')->findOrFail($id);
        $course->modules->makeVisible(['problems_count', 'solved_problems_count']);
        return $course;
    }
}