<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/2/17
 * Time: 7:46 PM
 */

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function index(Request $request)
    {
        return Language::all();
    }

    public function translations(Request $request)
    {
        $language = Language::where('code', app()->getLocale())->first();
        if(!$language) {
            $language = Language::where('code', 'en')->first();
        }
        return response($language->staticText)
            ->header('Content-Type', 'application/json');
    }
}