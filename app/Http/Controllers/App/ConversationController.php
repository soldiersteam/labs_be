<?php

/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/2/17
 * Time: 7:46 PM
 */

namespace App\Http\Controllers\App;

use App\Contracts\ConversationServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    protected $conversationService;

    public function __construct(ConversationServiceContract $conversationService)
    {
        $this->conversationService = $conversationService;
    }

    public function index(Request $request)
    {
        $course = $request->get('course');
        if ($request->get('pending') === 'true') {
            return $this->conversationService->getPendingConversations(Auth::user()->id, $course, 10);
        }
        return $this->conversationService->getUserConversations(Auth::user()->id, $course, 10);
    }

    public function send(Request $request, $id)
    {
        return $this->conversationService->send(Auth::user()->id, $id, $request->get('text'))->load('user');
    }

    public function show(Request $request, $id)
    {
        return $this->conversationService->getForUser($id, Auth::user()->id);
    }

    public function getMessages(Request $request, $id)
    {
        return $this->conversationService->getMessages($id, Auth::user()->id, $request->get('before'), 20);
    }

    public function read(Request $request, $id)
    {
        $this->conversationService->markConversationAsReadForUser($id, Auth::user()->id);
    }

    public function writeToUser(Request $request)
    {
        return $this->conversationService->writeToUser(Auth::user()->id, $request->get('toUserId'), $request->get('text'));
    }

    public function discuss(MessageRequest $request)
    {
        return $this->conversationService->discuss(Auth::user()->id, $request->get('text'));
    }
}
