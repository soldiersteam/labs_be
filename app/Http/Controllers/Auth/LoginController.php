<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        sendLoginResponse as protected traitSendLoginResponse;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function sendLoginResponse(Request $request)
    {
        if ($this->guard()->user()->hasRole(User::ROLE_ADMIN)) {
            return $this->traitSendLoginResponse($request);
        }
        if ($this->guard()->user()->hasRole(User::ROLE_PARENT)) {
            return $this->traitSendLoginResponse($request);
        }

        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();
        return $this->sendFailedLoginResponse($request);
    }
}
