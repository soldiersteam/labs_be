<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

abstract class AuthenticatableController extends Controller
{

    public function getToken(Request $request)
    {
        $this->checkToken();
        return ['api_token' => Auth::user()->api_token];
    }

    protected function checkToken()
    {
        if (!Auth::user()->isTokenValid()) {
            Auth::user()->api_token = Auth::user()->generateApiToken();
            Auth::user()->save();
        }
    }
}
