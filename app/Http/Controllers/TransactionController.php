<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 7/2/17
 * Time: 12:23 AM
 */

namespace App\Http\Controllers;


use App\Support\ChildService;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    protected $childService;

    function __construct(ChildService $childService)
    {
        $this->childService = $childService;
    }

    public function callback(Request $request)
    {
        $private_key = env('LIQPAY_PRIVATE_KEY');
        if (base64_encode(sha1($private_key . $request->data . $private_key, 1)) !== $request->signature) {
            abort(403, 'Forbidden');
        }
        $data = json_decode(base64_decode($request->data), true);
        $order_data = Transaction::getOrderIdData($data['order_id']);
        $transaction = Transaction::findOrFail($order_data['id']);

        $transaction->fill($data);
        $transaction->save();

        if ($transaction->isSuccessful()) {
            $this->childService->attachCourse($order_data['student_id'], $order_data['course_id']);
        }
    }
}