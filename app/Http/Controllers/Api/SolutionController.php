<?php

namespace App\Http\Controllers\Api;

use App\Events\SolutionTested;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SolutionPatchRequest;
use App\Solution;
use App\SolutionReport;
use App\Status;
use App\Support\PatchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class SolutionController extends Controller
{
    protected $patchService;

    public function __construct(PatchService $patchService)
    {
        $this->patchService = $patchService;
    }

    /**
     * Gets the latest unreserved unprocessed solution
     *
     * @param Request $request
     * @return Solution
     */
    public function latest_new(Request $request)
    {
        $solution = Solution::oldestNew();
        $solution->state = Solution::STATE_RESERVED;
        $solution->testing_server_id = \Auth::id();
        $solution->save();
        return $solution->makeHidden('code');
    }

    /**
     * Gets a solution
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function show(Request $request, $id)
    {
        return Solution::findOrFail($id)->makeHidden('code');
    }

    /**
     * Gets a solution's source code
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function show_source_code(Request $request, $id)
    {
        return Solution::where('id', $id)->firstOrFail()->code;
    }

    /**
     * Updates a solution
     *
     * @param SolutionPatchRequest $request
     * @param $id
     * @return mixed
     */
    public function update(SolutionPatchRequest $request, $id)
    {
        $this->patchService
            ->patch(Solution::where('id', $id)->firstOrFail(), $request->all());
        return Response::make();
    }

    /**
     * Stores a report to a solution
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    //@todo: fix solution statuses, add testing server id to solution
    public function store_report(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'in:' . implode(',', Solution::getStatuses()),
            'message' => 'string',//@todo max:xK
            'tests.*.status' => 'in:' . implode(',', SolutionReport::getStatuses()),
            'tests.*.execution_time' => 'numeric',
            'tests.*.memory_peak' => 'numeric',
        ]);

        $solution_reports = $request->get('tests', []);
        $reports = [];

        $reports_statuses_count = [
            'OK' => 0
        ];

        foreach ($solution_reports as $report) {
            $reports[] = new SolutionReport([
                'status' => $report['status'],
                'execution_time' => $report['execution_time'],
                'memory_peak' => $report['memory_peak'],
                'extra' => $report['extra'],
            ]);

            if(!array_key_exists($report['status'], $reports_statuses_count)) {
                $reports_statuses_count[$report['status']] = 0;
            }
            $reports_statuses_count[$report['status']]++;
        }

        $solution = Solution::where('id', $id)->firstOrFail();
        $solution->state = Solution::STATE_TESTED;

        $solution->success_percentage = 0;
        if(count($solution_reports)) {
            $solution->success_percentage = $reports_statuses_count['OK'] * 100 / count($solution_reports);
        }

        if($reports_statuses_count['OK'] == 0) {
            unset($reports_statuses_count['OK']);
        }
        if($request->has('status')) {
            $solution->statuses()->sync(Status::whereCode($request->get('status'))->get());
        } else {
            $solution->statuses()->sync(Status::whereIn('code', array_keys($reports_statuses_count))->get());
        }

        $solution->message = substr($request->get('message', ''), 0, 250); //@todo fix
        if($reports) {
            $solution->reports()->saveMany($reports);
        }
        $solution->save();

        broadcast(new SolutionTested($solution));

        return Response::make();
    }
}
 