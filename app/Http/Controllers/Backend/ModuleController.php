<?php

namespace App\Http\Controllers\Backend;

use App\Course;
use App\Http\Requests\Admin\ModuleRequest;
use App\Module;
use App\Support\LanguageService;
use App\Support\ModuleService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{

    private $moduleService;

    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
    }

    public function index(Request $request, $course_id = null)
    {
        return view('admin.modules.index', [
            'modules' => Module::where('course_id', $course_id)
                ->where(function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->get('search') . '%')
                        ->orWhere('description', 'like', '%' . $request->get('search') . '%');
                })
                ->paginate(10)
        ]);
    }

    public function showForm(Request $request, $course_id = null, $id = null)
    {
        $module = $id ? Module::findOrFail($id) : new Module;
        return view('admin.modules.form', [
            'module'    => $module,
            'languages' => app(LanguageService::class)->getEditFormList($module)
        ]);
    }

    public function update(ModuleRequest $request, $course_id, $id)
    {
        /** @var Module $module */
        $module = Module::findOrFail($id);

        /** @var Course $course */
        $course = Course::findOrFail($course_id);

        $this->moduleService->createOrUpdate($request->only(['name', 'description']), $module, $course);
        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Модуль обновлен успешно'
        ]);
    }

    public function create(ModuleRequest $request, $course_id)
    {
        /** @var Course $course */
        $course = Course::findOrFail($course_id);
        $this->moduleService->createOrUpdate($request->only(['name', 'description']), new Module(), $course);

        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Модуль создан успешно'
        ]);

        return redirect()->to(action('Backend\ModuleController@index', [$course_id]));
    }

    public function delete(Request $request, $id)
    {
        $module = Module::findOrFail($id);

        $course = $module->course;

        $module->delete();
        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Модуль удален успешно'
        ]);

        return redirect()->to(action('Backend\ModuleController@index', [$course->id]));
    }


}