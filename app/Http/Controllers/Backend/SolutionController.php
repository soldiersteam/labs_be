<?php

namespace App\Http\Controllers\Backend;

use App\Solution;
use App\Course;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SolutionController extends Controller
{
    public function index(Request $request) {
        $modules_selected = $request->get('modules') ? Module::whereIn('id',$request->get('modules'))->get(): [] ;
        $courses_selected = $request->get('courses') ? Course::whereIn('id',$request->get('courses'))->get(): [] ;  
        $modules_list = $request->get('modules') ? Module::whereNotIn('id',$request->get('modules'))->get(): Module::get();
        $courses_list = $request->get('courses') ? Course::whereNotIn('id',$request->get('courses'))->get(): Course::get();
        $solutions = Solution::join('modules', 'solutions.module_id', '=', 'modules.id')
            ->when($modules_selected, function ($query) use ($request) {
                    return $query->whereIn('module_id', $request->get('modules'));
                })
            ->when($courses_selected, function ($query) use ($request) {
                    return $query->whereIn('course_id', $request->get('courses'));
                })
            ->when($request->get('search', ''), function ($query) use ($request) {
                    return $query->where('message', 'like', '%' . $request->get('search', '') . '%')
                                ->orWhere('testing_mode', 'like', '%' . $request->get('search', '') . '%') 
                                ->orWhere('solutions.id', 'like', '%' . $request->get('search', '') . '%'); 
                })

            ->paginate(10);
        return view('admin.solution.index', [
          'solutions' => $solutions
        , 'courses_list' => $courses_list
        , 'modules_list' => $modules_list
        , 'modules_selected' => $modules_selected
        , 'courses_selected' => $courses_selected]);
    }

    public function showForm(Request $request, $id = null) {
        return view('admin.solution.form', [
            'solution' => $id ? Solution::findOrFail($id) : null
        ]);
    }

    public function update(Request $request, $id) {
        return $this->create($request, $id);
    }
    public function create(Request $request, $id = null) {
        //@todo: add validations

        $solution = $id ? Solution::findOrFail($id) : new Solution();

        $solution->fill($request->all());
        $solution->message = $request->message;
        $solution->save();
       
        \Session::flash('alert', [
            'type' => 'success',
            'message' => $id ? 'Решение обновлено успешно' : 'Решение создано успешно' 
        ]);

        return redirect()->to(action('Backend\SolutionController@index'));
    }
   
   public function delete(Request $request, $id) {

        Solution::findOrFail($id)->delete();

        \Session::flash('alert', [
            'type' => 'success',
            'message' => 'Решение успешно удалено'
        ]);

        return redirect()->to(action('Backend\SolutionController@index'));
    }

  
}
	