<?php

namespace App\Http\Controllers\Backend;

use App\ProgrammingLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgrammingLanguageController extends Controller
{
    public function index(Request $request) {
        return view('admin.programmingLanguage.index', [
            'languages' => ProgrammingLanguage::where('title', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('ace_mode', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('compiler_docker_image_name', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('executor_docker_image_name', 'like', '%' . $request->get('search', '') . '%')
                ->paginate(20)
        ]);
    }

    public function showForm(Request $request, $id = null) {
        return view('admin.programmingLanguage.form', [
            'language' => $id ? ProgrammingLanguage::findOrFail($id) : null
        ]);
    }

    public function update(Request $request, $id) {
        return $this->create($request, $id);
    }

    public function create(Request $request, $id = null) {
        $language = $id ? ProgrammingLanguage::findOrFail($id) : new ProgrammingLanguage();

        $this->validate($request, [
            'title' => 'required|max:255',
            'ace_mode' => 'required|max:255',
            'compiler_docker_image_name' => 'required|max:255',
            'executor_docker_image_name' => 'required|max:255'

        ]);

        $language->fill($request->all());

        $language->save();

        \Session::flash('alert', [
            'type' => 'success',
            'message' => $id ? 'Язык обновлен успешно' : 'Язык создан успешно'
        ]);

        return redirect()->to(action('Backend\ProgrammingLanguageController@index'));
    }

    public function delete(Request $request, $id) {

        ProgrammingLanguage::findOrFail($id)->delete();
        \Session::flash('alert', [
            'type' => 'success',
            'message' => 'Язык удален успешно'
        ]);

        return redirect()->to(action('Backend\ProgrammingLanguageController@index'));
    }
}
