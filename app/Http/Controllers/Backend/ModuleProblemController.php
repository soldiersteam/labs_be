<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ModuleProblemProgrammingLanguage;
use App\Problem;
use App\Module;
use App\ProgrammingLanguage;
use Illuminate\Http\Request;
use League\Flysystem\Exception;

class ModuleProblemController extends Controller
{
    public function index(Request $request, $course_id, $module_id) {
        $problems = Module::findOrFail($module_id)
            ->problems()
            ->withProgrammingLanguages($module_id)
            ->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->get('search', '') . '%')
                    ->orWhere('id', 'like', '%' . $request->get('search', '') . '%')
                    ->orWhere('display_id', 'like', '%' . $request->get('search', '') . '%');
            })
            ->get();
        $problems_ids = [];
        foreach ($problems as $problem) {
            $problems_ids[] = $problem->id;
        }

        return view('admin.module_problems.index', [//@todo:add search\
            'programming_languages' => ProgrammingLanguage::get(),
            'problems'   => $problems,
            'problems_select'   => Problem::whereNotIn('id', $problems_ids)->get(),
        ]);
    }
    public function add(Request $request, $course_id, $module_id) {

        $module = Module::findOrFail($module_id);

        try {
            $module->problems()->attach($request->get('problems'));
            \Session::flash('alert', [
                'type' => 'success',
                'message' => 'Задачи успешно добавлены'
            ]);
        } catch (Exception $exception) {
            \Session::flash('alert', [
                'type' => 'danger',
                'message' => 'Задачи не добавлены'
            ]);
        }

        return redirect()->to(action('Backend\ModuleProblemController@index', [$course_id, $module_id]));
    }
    public function delete(Request $request, $id, $module_id, $problem_id) {
        $module = Module::findOrFail($module_id);

        $module->problems()->detach($problem_id);
        \Session::flash('alert', [
            'type' => 'success',
            'message' => 'Задача удалена успешно'
        ]);

        return redirect()->to(action('Backend\ModuleProblemController@index', [$id, $module_id]));
    }
    public function edit(Request $request, $id, $module_id) {

        foreach ($request->get('display_ids', []) as $problem_id => $display_id) {
            Module::findOrFail($module_id)
                ->problems()
                ->updateExistingPivot($problem_id, ['display_id' => $display_id]);
        }

        ModuleProblemProgrammingLanguage::where([
            'module_id' => $module_id
        ])->delete();

        foreach ($request->get('allowed_languages', []) as $problem_id => $langsPack) {
            foreach ($langsPack as $lang) {
                ModuleProblemProgrammingLanguage::create([
                    'module_id' => $module_id,
                    'problem_id' => $problem_id,
                    'programming_language_id' => $lang,
                ]);
            }
        }
        \Session::flash('alert', [
            'type' => 'success',
            'message' => 'Задачи успешно обновлены'
        ]);

        return redirect()->to(action('Backend\ModuleProblemController@index', [$id, $module_id]));
    }
}
