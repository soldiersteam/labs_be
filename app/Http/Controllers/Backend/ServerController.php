<?php

namespace App\Http\Controllers\Backend;

use App\TestingServer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerController extends Controller
{
    public function index(Request $request) {
        return view('admin.server.index', [
            'servers' => TestingServer::where('name', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('login', 'like', '%' . $request->get('search', '') . '%')
                ->paginate(10)

        ]);
    }

    public function showForm(Request $request, $id = null) {
        return view('admin.server.form', [
            'server' => $id ? TestingServer::findOrFail($id) : null
        ]);
    }
    public function update(Request $request, $id) {
        return $this->create($request, $id);
    }

    public function create(Request $request, $id = null) {
        $server = $id ? TestingServer::findOrFail($id) : new TestingServer();

        $this->validate($request, [
            'name'     => 'required|max:255',
            'login'    => 'required|max:255',
            'password' => 'required|max:255',
        ]);

        $server->fill($request->all());
        $server->password = $request->get('password');

        $server->save();

        \Session::flash('alert', [
            'type' => 'success',
            'message' => $id ? 'Сервер обновлен успешно' : 'Сервер создан успешно'
        ]);

        return redirect()->to(action('Backend\ServerController@index'));
    }

    public function delete(Request $request, $id) {

        TestingServer::findOrFail($id)->delete();
        \Session::flash('alert', [
            'type' => 'success',
            'message' => 'Сервер удален успешно'
        ]);

        return redirect()->to(action('Backend\ServerController@index'));
    }
}
