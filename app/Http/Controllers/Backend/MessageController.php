<?php

namespace App\Http\Controllers\Backend;

use App\Message;
use App\Conversation;
use App\UsersConversation;
use App\MessageAttachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $conversations = Conversation::paginate(10);

        if ($request->get('conversation')) {
            $messages = Conversation::find($request->get('conversation'))->messages()
                ->orderBy('messages.created_at')
                ->paginate(10);
        } else {
            $messages = null;
        }

        return view('admin.messages.index', [
            'conversations' => $conversations,
            'messages' => $messages
        ]);
    }
}
