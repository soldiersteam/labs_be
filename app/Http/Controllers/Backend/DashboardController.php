<?php

namespace App\Http\Controllers\Backend;

use App\Solution;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function moneyChartData(Request $request) {
        $this->validate($request, [
            'from' => 'required|date',
            'to'   => 'required|date',
        ]);
        return Transaction::select(['amount', 'updated_at'])
            ->where('status', Transaction::STATUS_SUCCESS)
            ->where('updated_at', '>=', $request->get('from'))
            ->where('updated_at', '<=', $request->get('to'))
            ->get();
    }

    public function userChartData(Request $request) {
        $this->validate($request, [
            'from' => 'required|date',
            'to'   => 'required|date',
        ]);
        return User::select(['id', 'created_at'])
            ->where('updated_at', '>=', $request->get('from'))
            ->where('updated_at', '<=', $request->get('to'))
            ->get();
    }

    public function solutionChartData(Request $request) {
        $this->validate($request, [
            'from' => 'required|date',
            'to'   => 'required|date',
        ]);
        return Solution::select(['id', 'created_at'])
            ->where('updated_at', '>=', $request->get('from'))
            ->where('updated_at', '<=', $request->get('to'))
            ->get();
    }

}
