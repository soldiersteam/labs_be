<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Admin\CreateProblemRequest;
use App\Http\Requests\Admin\UpdateProblemRequest;
use App\Problem;
use App\Support\LanguageService;
use App\Support\ProblemService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProblemController extends Controller
{
    private $problemService;
    private $languageService;

    public function __construct(ProblemService $problemService, LanguageService $languageService)
    {
        $this->problemService = $problemService;
        $this->languageService = $languageService;
    }

    public function index(Request $request)
    {
        return view('admin.problems.index', [
            'problems' => Problem::where('name', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('id', 'like', '%' . $request->get('search', '') . '%')
                ->paginate(10)
        ]);
    }

    public function showForm(Request $request, $id = null)
    {
        $problem = $id ? Problem::findOrFail($id) : new Problem();
        return view('admin.problems.form', [
            'problem'   => $problem,
            'languages' => $this->languageService->getEditFormList($problem)
        ]);
    }

    /**
     * @param UpdateProblemRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProblemRequest $request, $id)
    {
        /** @var Problem $problem */
        $problem = Problem::findOrFail($id);
        $this->problemService->createOrUpdate($request->except('archive'), $problem);

        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Задача обновлена успешно'
        ]);
        return redirect()->to(action('Backend\ProblemController@index'));
    }

    public function create(CreateProblemRequest $request)
    {
        $this->problemService->createOrUpdate($request->except('archive'), new Problem());

        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Задача создана успешно'
        ]);

        return redirect()->to(action('Backend\ProblemController@index'));
    }

    public function delete(Request $request, $id)
    {

        Problem::findOrFail($id)->delete();
        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Задача удалена успешно'
        ]);

        return redirect()->to(action('Backend\ProblemController@index'));
    }

    public function importProblems(Request $request)
    {

        try {
            $this->validate($request, [
                'problems'   => 'required',
                'problems.*' => 'mimetypes:application/x-gzip'
            ]);
            $this->problemService->importManyFromFile($request->problems);
            $count = count($request->problems);
            \Session::flash('alert', [
                'type'    => 'success',
                'message' => "Импортировали {$count} задач"
            ]);
        } catch (\Exception $exception) {
            \Session::flash('alert', [
                'type'    => 'danger',
                'message' => $exception->getMessage()
            ]);
        }
        return redirect()->to(action('Backend\ProblemController@index'));
    }
}
