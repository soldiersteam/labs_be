<?php

namespace App\Http\Controllers\Backend;

use App\Course;
use App\Language;
use App\Support\LanguageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    private $languageService;

    public function __construct(LanguageService $languageService)
    {
        $this->languageService = $languageService;
    }

    public function index(Request $request)
    {
        return view('admin.course.index', [
            'courses' => Course::paginate(10)
        ]);
    }

    public function showForm(Request $request, $id = null)
    {
        $course = $id ? Course::findOrFail($id) : new Course(['price' => '']);
        return view('admin.course.form', [
            'course'    => $course,
            'languages' => $this->languageService->getEditFormList($course)
        ]);
    }

    public function update(Request $request, $id)
    {
        return $this->create($request, $id);
    }

    public function create(Request $request, $id = null)
    {
        $course = $id ? Course::findOrFail($id) : new Course();
        $this->validate($request, [
            'name.' . Language::ID_ORIGINAL        => 'required',
            'name.*'                               => 'max:255',
            'description.' . Language::ID_ORIGINAL => 'required',
            'description.*'                        => 'max:1000',
            'content.' . Language::ID_ORIGINAL     => 'required',
            'content.*'                            => 'max:10000',
            'duration'                             => 'required|numeric',
            'price'                                => 'required|numeric'
        ]);

        $this->languageService->fillModel($request->all(), $course);

        \Session::flash('alert', [
            'type'    => 'success',
            'message' => $id ? 'Курс обновлен успешно' : 'Курс создан успешно'
        ]);

        return redirect()->to(action('Backend\CourseController@index'));
    }

    public function delete(Request $request, $id)
    {

        Course::findOrFail($id)->delete();
        \Session::flash('alert', [
            'type'    => 'success',
            'message' => 'Курс удален успешно'
        ]);

        return redirect()->to(action('Backend\CourseController@index'));
    }
}
