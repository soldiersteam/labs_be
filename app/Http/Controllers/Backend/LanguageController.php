<?php

namespace App\Http\Controllers\Backend;

use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    public function index(Request $request) {
        return view('admin.language.index', [
            'languages' => Language::where('name', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('code', 'like', '%' . $request->get('search', '') . '%')
                ->orWhere('id', 'like', '%' . $request->get('search', '') . '%')
                ->paginate(10)
        ]);
    }

    public function showForm(Request $request, $id = null) {
        return view('admin.language.form', [
            'language' => $id ? Language::findOrFail($id) : null
        ]);
    }

    public function update(Request $request, $id) {
        return $this->create($request, $id);
    }

    public function create(Request $request, $id = null) {
        $language = $id ? Language::findOrFail($id) : new Language();

        $this->validate($request, [
            'name' => 'required|max:255',
            'code' => 'required|max:2'
        ]);

        $language->fill($request->all());
        $language->save();
        if($request->hasFile('translations_json')) {
            $language->staticTextFile = $request->file('translations_json');
        }
        \Session::flash('alert', [
            'type' => 'success',
            'message' => $id ? 'Язык обновлен успешно' : 'Язык создан успешно'
        ]);

        return redirect()->to(action('Backend\LanguageController@index'));
    }

    public function delete(Request $request, $id) {

        Language::findOrFail($id)->delete();
        \Session::flash('alert', [
            'type' => 'success',
            'message' => 'Язык удален успешно'
        ]);

        return redirect()->to(action('Backend\LanguageController@index'));
    }

    public function translations(Request $request, $id) {
        return view('admin.language.translations');
    }

    public function exportTranslations(Request $request, $id) {
        $lang = Language::findOrFail($id);

        return response($lang->staticText)
            ->header('Content-Disposition', 'attachment; filename="' . time() . '_' . $lang->code . '_' . 'translations.json"')
            ->header('Content-Type', 'application/json');
    }
}
