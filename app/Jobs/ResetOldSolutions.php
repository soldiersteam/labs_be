<?php

namespace App\Jobs;

use App\Solution;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ResetOldSolutions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Solution::whereNotIn('state', [Solution::STATE_REJECTED, Solution::STATE_NEW, Solution::STATE_TESTED])
            ->where('updated_at', '<', Carbon::now()->subMinutes(config('configs.reset_solutions_older_than')))
            ->update(['state' => Solution::STATE_NEW]);
    }
}
