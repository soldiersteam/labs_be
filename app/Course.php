<?php

namespace App;

use App\Scopes\WithTranslationsScope;
use App\Support\CourseService;
use App\Support\Exchange;
use App\Support\TranslatableFormAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Course
 *
 * @property-read mixed $about_link
 * @property-read mixed $buy_link
 * @property-read mixed $link
 * @property-read mixed $price
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Module[] $modules
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $content
 * @property int $duration
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereDuration($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Course whereUpdatedAt($value)
 * @property-read mixed $price_u_s_d
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $requiredCourses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Translation[] $translations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 */
class Course extends TranslatableModel
{
    use SoftDeletes;

    public function getTranslatableAttributes(): array
    {
        return [
            'name'        => new TranslatableFormAttribute('Name'),
            'description' => new TranslatableFormAttribute('Description', TranslatableFormAttribute::INPUT_TYPE_TEXTAREA),
            'content'     => new TranslatableFormAttribute('Content', TranslatableFormAttribute::INPUT_TYPE_TEXTAREA),
        ];
    }

    protected $visible = ['id', 'name', 'price', 'description', 'content', 'modules'];
    protected $fillable = ['name', 'description', 'content', 'duration', 'price'];

    public function getAboutLinkAttribute()
    {
        return action('CourseController@about', ['course_id' => $this->id]);
    }

    public function getBuyLinkAttribute()
    {
        return action('CourseController@buy', ['course_id' => $this->id]);
    }

    public function getLinkAttribute()
    {
        return action('CourseController@show', ['course_id' => $this->id]);
    }

    public function getPriceAttribute()
    {
        return Exchange::rate() * $this->attributes['price'];
    }

    public function getPriceUSDAttribute()
    {
        return $this->attributes['price'];
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function requiredCourses()
    {
        return $this->belongsToMany(Course::class, 'course_requirements', 'course_id', 'required_course_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps()->withPivot('completed');
    }

    public function solutions()
    {
        return $this->hasMany(Solution::class);
    }

}
