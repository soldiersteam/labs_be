<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

/**
 * App\Solution
 *
 * @property integer $id
 * @property string $state
 * @property string $testing_mode
 * @property string $message
 * @property integer $problem_id
 * @property integer $programming_language_id
 * @property integer $user_id
 * @property integer $testing_server_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $success_percentage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SolutionReport[] $reports
 * @property-read \App\Problem $problem
 * @property-read \App\User $user
 * @property-read \App\ProgrammingLanguage $programming_language
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereTestingMode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereProblemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereProgrammingLanguageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereTestingServerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereSuccessPercentage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution oldestNew()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SolutionMessage[] $messages
 * @property int $contest_id
 * @property int $client_id
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereContestId($value)
 * @property int $module_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Conversation[] $conversations
 * @property-read mixed $code
 * @property-read mixed $link
 * @property-read mixed $max_memory
 * @property-read mixed $max_time
 * @property-read mixed $successful_reports
 * @property-read \App\Module $module
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Status[] $statuses
 * @method static \Illuminate\Database\Query\Builder|\App\Solution whereModuleId($value)
 */
class Solution extends Model
{
    use SolutionRelations;
    protected $with = ['statuses', 'programming_language'];

    protected $appends = ['code', 'max_memory', 'successful_reports', 'max_time'];
    protected $hidden = ['max_memory', 'max_time', 'successful_reports'];

    const STATE_NEW = 'new';
    const STATE_RECEIVED = 'received';
    const STATE_REJECTED = 'rejected';
    const STATE_RESERVED = 'reserved';
    const STATE_TESTED = 'tested';

    const STATUS_CE = 'CE';
    const STATUS_FF = 'FF';
    const STATUS_NC = 'NC';
    const STATUS_CC = 'CC';
    const STATUS_CT = 'CT';
    const STATUS_UE = 'UE';
    const STATUS_OK = 'OK';
    const STATUS_WA = 'WA';
    const STATUS_PE = 'PE';
    const STATUS_RE = 'RE';
    const STATUS_TL = 'TL';
    const STATUS_ML = 'ML';
    const STATUS_ZR = 'ZR';

    protected $fillable = ['state', 'course_id', 'module_id', 'problem_id', 'programming_language_id'];

    public static function getValidationRules($contest_id)
    {
        return [
            'programming_language' => 'required|exists:module_programming_language,programming_language_id,module_id,' . $contest_id,
            'solution_code'        => 'required_without:solution_code_file',
            'solution_code_file'   => 'required_without:solution_code|mimetypes:text/plain',
        ];
    }

    public function getLinkAttribute()
    {
        return action('SolutionController@show', [
            'solution_id' => $this->id,
            'problem_id'  => $this->problem_id,
            'module_id'   => \Request::route()->parameters()['module_id'],
            'course_id'   => \Request::route()->parameters()['course_id'],
        ]);
    }

    /*
     * разбиваем дату создания на год, месяц, день
     *
     * и сохраняем по такому пути solutions_source_code/год/месяц/день/id
     * @todo: make path creation more beautiful
     */
    public function sourceCodePath()
    {
        $dir = 'solutions_source_code/' .
            $this->created_at->year . '/' .
            $this->created_at->month . '/' .
            $this->created_at->day . '/';
        $dir = storage_path($dir);

        if (!File::exists($dir)) {
            File::makeDirectory($dir, 0755, true);
        }
        return $dir;
    }

    public function sourceCodeFilePath()
    {
        return $this->sourceCodePath() . $this->id;
    }

    /**
     * Scope a query to only include oldest new solution.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOldestNew($query)
    {
        return $query->where('state', self::STATE_NEW)
            ->orderBy('created_at', 'asc')
            ->with('programming_language')
            ->firstOrFail();
    }

    public static function getStates()
    {
        return [
            self::STATE_NEW,
            self::STATE_RECEIVED,
            self::STATE_REJECTED,
            self::STATE_RESERVED,
            self::STATE_TESTED,
        ];
    }

    public function getPoints()
    {
        return $this->attributes['success_percentage'];
    }

    public static function getStatusDescriptions()
    {
        return [
            self::STATUS_OK => 'Test Passed',
            self::STATUS_CE => 'Compilation Error',
            self::STATUS_FF => 'Forbidden Function',
            self::STATUS_NC => 'No Checker found',
            self::STATUS_CC => 'Checker Crashed',
            self::STATUS_CT => 'Checker Timed out',
            self::STATUS_UE => 'Unknown Error',
            self::STATUS_ZR => 'Annulled',
            self::STATUS_PE => 'Presentation Error',
            self::STATUS_RE => 'Runtime Error',
            self::STATUS_TL => 'Time Limit',
            self::STATUS_ML => 'Memory Limit',
        ];
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_OK,
            self::STATUS_CE,
            self::STATUS_FF,
            self::STATUS_NC,
            self::STATUS_CC,
            self::STATUS_CT,
            self::STATUS_UE,
            self::STATUS_ZR,
            self::STATUS_WA,
            self::STATUS_PE,
            self::STATUS_RE,
            self::STATUS_TL,
            self::STATUS_ML,
        ];
    }

    public function scopeSolved($query)
    {
        $query->where('success_percentage', 100);
    }

    public function saveCodeFile($file)
    {
        if (Input::file($file)->isValid()) {
            Input::file($file)->move($this->sourceCodePath(), $this->id);
            File::copy($this->sourceCodePath() . $this->id, $this->getAlternatePath() . $this->getAlternateFilename());
        }
    }

    public function getCodeAttribute()
    {
        if (!File::exists($this->sourceCodeFilePath())) {
            return '';
        }

        return File::get($this->sourceCodeFilePath());
    }

    public function annul()
    {
        $this->status = static::STATUS_ZR;
        $this->success_percentage = 0;
    }

    public static function getList($per_page, $module_id, $problem_ids, $user_id = null)
    {
        $query = static::query();
        if ($module_id) {
            $query->where('solutions.module_id', $module_id);
        }
        if ($problem_ids) {
            $query->whereIn('solutions.problem_id', $problem_ids);
        }
        if ($user_id) {
            $query->where('solutions.user_id', $user_id);
        }
        $query->select('solutions.*')
            ->leftJoin('solutions as s', function ($join) {
                $join->on('s.problem_id', '=', 'solutions.problem_id')
                    ->on('solutions.created_at', '<', 's.created_at');
            })
            ->whereNull('s.created_at');
        if ($problem_ids) {
            return $query->get();
        }
        $query->paginate($per_page);
    }

    public function getMaxMemoryAttribute()
    {
        $max = 0;
        foreach ($this->reports as $report) {
            $max = $max > $report->memory_peak ? $max : $report->memory_peak;
        }
        return $max;
    }

    public function getMaxTimeAttribute()
    {
        $max = 0;
        foreach ($this->reports as $report) {
            $max = $max > $report->execution_time ? $max : $report->execution_time;
        }
        return $max;
    }

    public function getSuccessfulReportsAttribute()
    {
        $count = 0;
        foreach ($this->reports as $report) {
            $count += (int)($report->status == SolutionReport::STATUS_OK);
        }
        return $count;
    }
}
