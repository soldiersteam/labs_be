<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Message
 *
 * @property int $id
 * @property string $message
 * @property bool $is_read
 * @property int $conversation_id
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MessageAttachment[] $attachments
 * @property-read \App\Conversation $conversation
 * @property-read mixed $username
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereConversationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereIsRead($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUserId($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $fillable = ['message'];

    public function attachments()
    {
        return $this->hasMany(MessageAttachment::class);
    }

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function topic()
    {
        return $this->morphTo();
    }

    public function problem()
    {
        return $this->belongsTo(Problem::class, 'topic_id');
    }

    public function solution()
    {
        return $this->belongsTo(Solution::class, 'topic_id');
    }
}
