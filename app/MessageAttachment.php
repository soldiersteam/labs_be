<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MessageAttachment
 *
 * @property int $id
 * @property int $message_id
 * @property string $path
 * @property int $size
 * @property string $extention
 * @property string $file_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereExtention($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereMessageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageAttachment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MessageAttachment extends Model
{
    
}
