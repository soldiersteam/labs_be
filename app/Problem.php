<?php

namespace App;

use App\Support\TranslatableFormAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

/**
 * Class Problem
 *
 * @package App
 * @property integer $id
 * @property string $name
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $archive
 * @property string $description
 * @property boolean $difficulty
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereArchive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Problem whereDifficulty($value)
 * @mixin \Eloquent
 * @property-read mixed $image
 * @property-read mixed $display_id
 * @property-read mixed $last_solution
 * @property-read mixed $link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Module[] $modules
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProgrammingLanguage[] $programming_languages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Solution[] $solutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Translation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Problem withLatestSolutions()
 * @method static \Illuminate\Database\Query\Builder|\App\Problem withProgrammingLanguages($module_id)
 */
class Problem extends TranslatableModel
{
    use SoftDeletes;
    use Sortable;

    public $max_points;
    public $review_required;
    public $time_penalty;

    const RESULTS_PER_PAGE = 10;
    protected static $sortable_columns = [
        'id', 'name', 'created_at', 'updated_at', 'deleted_at', 'difficulty',
    ];

    public $fillable = [
        'name', 'difficulty', 'archive'
    ];

    public $hidden = [
        'archive', 'display_id', 'pivot', 'image'
    ];

    protected $appends = ['display_id', 'image'];

    public function getTranslatableAttributes(): array
    {
        return ['name' => new TranslatableFormAttribute('Name')];
    }

    public function solutions()
    {
        return $this->hasMany(Solution::class);
    }

    public static function getValidationRules()
    {
        return [
            'name'       => 'required|string|max:255',
            'difficulty' => 'required|integer|between:0,5',
            'archive'    => 'required,mimetypes:application/x-gzip',
            //'volumes' => 'array'
        ];
    }


    public function getFilePath($absolute_path = false)
    {
        $relative_path = 'problems/' . $this->id . '/';
        $dir = storage_path('app/' . $relative_path);
        if (!File::exists($dir)) {
            File::makeDirectory($dir, 0755, true);
        }
        if ($absolute_path) {
            return $dir;
        }
        return $relative_path;
    }

    public function getImagePath()
    {
        $dir = public_path('problemdata/' . $this->id . '/');
        if (!File::exists($dir)) {
            File::makeDirectory($dir, 0755, true);
        }

        return $dir;
    }

    public function getImageAttribute()
    {
        if (!File::exists($this->getImagePath() . 'image.ru.png')) {
            return asset('frontend-bundle/media/no-problem-image.png');
        }
        //todo
        return asset('problemdata/' . $this->id . '/image.' . app()->getLocale() . '.png');
    }

    public function getLastSolutionAttribute()
    {
        return $this->solutions()
            ->where('user_id', \Auth::id())
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function getLinkAttribute()
    {
        return action('ProblemController@show', [
            'problem_id' => $this->id,
            'module_id'  => \Request::route()->parameters()['module_id'],
            'course_id'  => \Request::route()->parameters()['course_id'],
        ]);
    }

    public function setArchive(UploadedFile $file)
    {
        if ($file->isValid()) {
            if (isset($this->attributes['archive'])) {
                File::delete($this->getFilePath(true) . $this->attributes['archive']);
            }
            $this->attributes['archive'] = $file->getClientOriginalName();
            $file->storeAs($this->getFilePath(false), $this->attributes['archive']);
        }
    }

    public function getArchiveAttribute($value)
    {
        try {
            return File::get($this->getFilePath() . $value);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getArchivePath($absolute = false)
    {
        return $this->getFilePath($absolute) . $this->attributes['archive'];
    }

    public static function search($term, $page)
    {
        $problems = static::select(['id', 'name'])
            ->where('name', 'LIKE', '%' . $term . '%');

        $count = $problems->count();
        $problems = $problems->skip(($page - 1) * static::RESULTS_PER_PAGE)
            ->take(static::RESULTS_PER_PAGE)
            ->get();
        return ['results' => $problems, 'total_count' => $count];
    }

    /*
     * AHTUNG!!!!
     * USE scopeWithProgrammingLanguages
     */
    public function programming_languages()
    {
        return $this->belongsToMany(ProgrammingLanguage::class, 'module_problem_programming_language');
    }

    public function scopeWithProgrammingLanguages($query, $module_id)
    {
        return $query->with(['programming_languages' => function ($q) use ($module_id) {
            return $q->where('module_id', $module_id);
        }]);
    }

    public function scopeWithLatestSolutions($query, $userId = null, $limit = true)
    {
        return $query->with(['solutions' => function ($q) use ($userId, $limit) {
            if ($userId) {
                $q->where('user_id', $userId);
            }

            if ($limit) {
                $q->orderBy('created_at', 'desc')
                    ->groupBy(\DB::raw('problem_id DESC'));
            }
        }]);
    }

    public function scopeWithBestSolutions($query, $userId)
    {
        return $query->with(['solutions' => function ($q) use ($userId) {
            $q->where('user_id', $userId);
            $q->orderBy('success_percentage', 'desc')
                ->orderBy('created_at', 'desc')
                ->groupBy(\DB::raw('problem_id DESC'));
        }]);
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class)->withPivot('display_id');
    }

    public function getDisplayIdAttribute()
    {
        return $this->pivot->display_id;
    }
}
