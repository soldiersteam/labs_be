<?php

namespace App\Events;

use App\Conversation;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConversationRead implements ShouldBroadcast, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $conversation;
    public $readById;

    /**
     * Create a new event instance.
     * @param Conversation $conversation
     * @param int $readById
     */
    public function __construct(Conversation $conversation, $readById)
    {
        $this->conversation = $conversation;
        $this->readById = $readById;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return $this->conversation->users->map(function (User $user) {
            return new PrivateChannel("user.$user->id");
        })->toArray();
    }

    public function broadcastWith()
    {
        return ['conversationId' => $this->conversation->id, 'readById' => $this->readById];
    }
}
