<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ValidationToken extends Model
{
    const TYPE_PHONE = 'phone';
    const TYPE_EMAIL = 'email';

    protected $fillable = ['token', 'type', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
