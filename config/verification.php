<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 11/16/17
 * Time: 1:57 AM
 */

return [
    'phone_token_length' => 6,
    // minutes until sms can be resent
    'phone_resend_timeout' => 15,
    // minutes until email can be resent
    'email_resend_timeout' => 15,
];