<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 9/27/17
 * Time: 7:14 PM
 */

return [
    'reset_solutions_older_than' => env('RESET_SOLUTIONS_OLDER_THAN', 10)
];